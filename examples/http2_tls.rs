use papero::{Config, Handler, http::Response, Router, Server, TlsConfig};
use std::path::PathBuf;

#[tokio::main]
async fn main() -> papero::Result<()> {
    let index = Handler::new(move |_req, stream| Box::pin(async move {
        stream
            .send(Response::builder().body("Hello TLS").unwrap())
            .await?;
        Ok(())
    }));

    let config = Config::new()
        .with_http_listen_addr("127.0.0.1:8080")
        .with_https_listen_addr("127.0.0.1:8081")
        .with_redirect_to_https(Some("https://localhost:8081".to_string()))
        .with_tls(TlsConfig {
            cert_path: PathBuf::from("examples/tls/cert.pem"),
            key_path: PathBuf::from("examples/tls/key.pem"),
        });

    let mut router = Router::new();
    router.get("/", index);

    let server = Server::new(config, router);

    println!("Server started at http://localhost:8080 or https://localhost:8081");
    server.run().await
}
