use papero::{Config, Handler, http::Response, Router, Server};

#[tokio::main]
async fn main() -> papero::Result<()> {
    let index = Handler::new(move |_req, stream| {
        Box::pin(async move {
            stream
                .send(Response::builder().body("Hello world!").unwrap())
                .await?;
            Ok(())
        })
    });

    let config = Config::new().with_http_listen_addr("127.0.0.1:8080");

    let mut router = Router::new();
    router.get("/", index);

    let server = Server::new(config, router);

    println!("Server started at http://localhost:8080");
    server.run().await
}
