//! Example of an API documented using OpenAPI and providing an endpoint with [RapiDoc](https://rapidocweb.com)
//! to see the OpenAPI documentation in the browser.
//!
//! Run with `cargo run --example openapi --features openapi`
use http::StatusCode;
use papero::{
    router::openapi::{self, OpenAPI},
    Config, Handler, Response, Router, Server,
};

#[tokio::main]
async fn main() -> papero::Result<()> {
    let index = Handler::new(move |_req, stream| {
        Box::pin(async move {
            stream
                .send(Response::builder().body("Hello world!").unwrap())
                .await?;
            Ok(())
        })
    });

    let config = Config::new().with_http_listen_addr("127.0.0.1:8080");

    let openapi = OpenAPI::new(
        openapi::Info::new("OpenAPI demo", "1.0").with_description("OpenAPI demonstration API"),
    );

    let mut router = Router::new().with_openapi_spec(openapi);
    router.get_openapi(
        "/",
        index,
        openapi::Operation::default()
            .with_description("The home page")
            .with_response(
                StatusCode::OK,
                openapi::OrReference::Value(
                    openapi::Response::new("Successful response").with_content(
                        "text/plain",
                        openapi::MediaType::default()
                            .with_single_example(serde_json::Value::from("Hello world!")),
                    ),
                ),
            ),
    );

    let openapi_gen = router.take_openapi_spec().unwrap();

    router.get(
        "/openapi/openapi.json",
        Handler::new(move |_req, stream: &mut papero::stream::Stream| {
            let openapi_gen = openapi_gen.clone();

            Box::pin(async move {
                stream
                    .send(
                        Response::builder()
                            .header(papero::http::header::CONTENT_TYPE, "application/json")
                            .body(serde_json::to_string(&openapi_gen).unwrap())
                            .unwrap(),
                    )
                    .await?;

                Ok(())
            })
        }),
    );

    router.get(
        "/openapi",
        Handler::new(|_req, stream| {
            Box::pin(async move {
                stream
                    .send(
                        Response::builder()
                            .header(papero::http::header::CONTENT_TYPE, "text/html")
                            .body(
                                r#" <!DOCTYPE HTML>
<html>
  <head>
    <meta charset="utf-8">
    <script type="module" src="https://unpkg.com/rapidoc/dist/rapidoc-min.js"></script>
  </head>
  <body>
    <rapi-doc spec-url = "http://localhost:8080/openapi/openapi.json"> </rapi-doc>
  </body>
</html>
"#,
                            )
                            .unwrap(),
                    )
                    .await?;

                Ok(())
            })
        }),
    );

    let server = Server::new(config, router);

    println!("Server started at http://localhost:8080, look at the API documentation at http://localhost:8080/openapi");
    server.run().await
}
