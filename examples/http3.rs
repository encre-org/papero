//! An example showcasing HTTP/3 capabilities of papero.
//!
//! **HTTP/3 support is highly experimental and should not be used in production yet**
//!
//! To try this example, you need to run the script located in `examples/http3/gen_certs.sh` to
//! because HTTP/3 requires a set of valid certificates including a trusted CA.
//!
//! Then you can launch Chromium using `chromium --enable-quic --quic-version=h3 --origin-to-force-quic-on=localhost:8082 --ignore-certificate-errors-spki-list=<spki_hash> https://localhost:8082`
use papero::{
    http::{header, Response, Version},
    Config, Handler, Router, Server, TlsConfig,
};
use std::path::PathBuf;

#[tokio::main]
async fn main() -> papero::Result<()> {
    let index = Handler::new(move |_req, stream| {
        Box::pin(async move {
            if stream.http_version() == Version::HTTP_3 {
                stream
                    .send(Response::builder().body("Hello HTTP/3").unwrap())
                    .await?;
            } else {
                stream
                    .send(
                        Response::builder()
                            .header(header::ALT_SVC, "h3=\":8082\"")
                            .body("Hello slow HTTP, advertising HTTP/3 on port 8082")
                            .unwrap(),
                    )
                    .await?;
            }

            Ok(())
        })
    });

    let config = Config::new()
        .with_http_listen_addr("127.0.0.1:8080")
        .with_https_listen_addr("127.0.0.1:8081")
        .with_redirect_to_https(Some("https://localhost:8081".to_string()))
        .with_tls(TlsConfig {
            cert_path: PathBuf::from("examples/http3/out/leaf_cert.pem"),
            key_path: PathBuf::from("examples/http3/out/leaf_cert.key"),
        })
        .with_http3_listen_addr("[::]:8082");

    let mut router = Router::new();
    router.get("/", index);

    let server = Server::new(config, router);

    println!("Server started at https://localhost:8082");
    server.run().await
}
