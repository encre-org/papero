use papero::{http::Response, Config, Handler, Router, Server};

#[tokio::main]
async fn main() -> papero::Result<()> {
    let hello = Handler::new(move |req, stream| {
        Box::pin(async move {
            let params = papero::extract::extract_params(&req);
            let name = params
                .get("name")
                .ok_or(papero::Error::other("name parameter should be present"))?;

            stream
                .send(Response::builder().body(format!("Hello {name}!")).unwrap())
                .await?;
            Ok(())
        })
    });

    let config = Config::new().with_http_listen_addr("127.0.0.1:8080");

    let mut router = Router::new();
    router.get("/hello/{name}", hello);

    let server = Server::new(config, router);

    println!("Server started at http://localhost:8080");
    server.run().await
}
