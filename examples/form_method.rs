use papero::{stream_responses::Html, Config, Handler, Router, Server};
use serde::Deserialize;

const INDEX_HTML: &str = r#"<!DOCTYPE HTML>
<html>
  <head>
    <meta charset="utf-8">
    <title>Form method example</title>
  </head>
  <body>
    <h1>File deletion</h1>
    <p style="font-size: 12px; color: gray;">
      This is a fake example, nothing will be deleted.
      Check the example source for more information.
    </p>

    <form method="POST" action="/?_method=DELETE">
      <input type="text" name="filename" placeholder="File to be deleted" required />
      <button style="border: none; background-color: red; padding: 0.25rem 0.5rem 0.25rem 0.5rem; border-radius: 4px;">DELETE</button>
    </form>
  </body>
</html>"#;

const INDEX_HTML_DELETE: &str = r#"<!DOCTYPE HTML>
<html>
  <head>
    <meta charset="utf-8">
    <title>Form method example</title>
  </head>
  <body>
    <h3>File {filename} deleted!</h3>
    <a href="/" style="background-color: blue; color: white; padding: 0.25rem 0.5rem 0.25rem 0.5rem; border-radius: 4px;">Back to home</a>
  </body>
</html>"#;

#[tokio::main]
async fn main() -> papero::Result<()> {
    let index = Handler::new(move |_req, stream| {
        Box::pin(async move {
            stream.send(Html::new(INDEX_HTML)).await?;
            Ok(())
        })
    });

    let index_delete = Handler::new(move |req, stream| {
        Box::pin(async move {
            #[derive(Deserialize)]
            struct FormData {
                filename: String,
            }

            let form: FormData = papero::extract::extract_urlencoded_body(&req)?;
            stream
                .send(Html::new(
                    INDEX_HTML_DELETE.replace("{filename}", &form.filename),
                ))
                .await?;
            Ok(())
        })
    });

    let config = Config::new().with_http_listen_addr("127.0.0.1:8080");

    let mut router = Router::new();
    router.get("/", index);
    router.delete("/", index_delete);

    let server = Server::new(config, router);

    println!("Server started at http://localhost:8080");
    server.run().await
}
