#!/bin/sh
rm -rf out
mkdir out
echo 01 > out/2048-sha256-root-serial
touch out/2048-sha256-root-index.txt

# Generate the key
openssl genrsa -out out/2048-sha256-root.key 2048

# Generate the root certificate
openssl req -new -key out/2048-sha256-root.key \
  -out out/2048-sha256-root.req \
  -config ca.cnf

openssl x509 \
  -req -days 3 \
  -in out/2048-sha256-root.req \
  -signkey out/2048-sha256-root.key \
  -extfile ca.cnf \
  -extensions ca_cert \
  -text > out/2048-sha256-root.pem

# Generate the leaf certificate request
openssl req \
  -new \
  -keyout out/leaf_cert.key \
  -out out/leaf_cert.req \
  -config leaf.cnf

# Convert the key to pkcs8
openssl pkcs8 \
  -topk8 \
  -outform DER \
  -inform PEM \
  -in out/leaf_cert.key \
  -out out/leaf_cert.pkcs8 \
  -nocrypt

# Generate the leaf certificate to be valid for three days
openssl ca \
  -batch \
  -days 3 \
  -extensions user_cert \
  -in out/leaf_cert.req \
  -out out/leaf_cert.pem \
  -config ca.cnf

spki_hash=$(openssl x509 -noout -pubkey < out/leaf_cert.pem | \
    openssl rsa -pubin -outform der | \
    openssl dgst -sha256 -binary | \
    openssl enc -base64)

echo "\nSPKI hash: $spki_hash"
echo "If you are running the \`http3\` example, you can launch Chromium using \`chromium --enable-quic --quic-version=h3 --origin-to-force-quic-on=localhost:8082 --ignore-certificate-errors-spki-list=$spki_hash https://localhost:8082\`"
