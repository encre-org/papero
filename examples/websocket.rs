use std::hash::{DefaultHasher, Hash, Hasher};

use futures::{SinkExt, StreamExt};
use papero::{stream_responses, ws::{Message, WebSocketStream}, Config, Handler, Router, Server};

const INDEX_HTML: &str = r##"<!DOCTYPE HTML>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>WebSocket Example</title>
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
  </head>
  <body>
    <h3>WebSocket hasher</h3>
    <p>Input a message in the text input below and it will be hashed server-side.</p>

    <div>
      <ul id="msgs"></ul>
      <input type="text" id="msg" placeholder="Enter a message" autocomplete="off" autofocus>
      <button type="button" id="submit">Hash</button>
    </div>

    <script>
      const ws = new WebSocket(location.protocol === "https://" ? "wss://" : "ws://" + location.host + "/ws");

      const sendMessage = () => {
        ws.send(msg.value);
        msg.value = "";
      };

      ws.addEventListener("message", (e) => {
        const li = document.createElement("li");
        li.innerText = e.data;
        msgs.appendChild(li);
      });

      submit.addEventListener("click", sendMessage);

      msg.addEventListener("keydown", (e) => {
        if (e.key === "Enter") {
          sendMessage();
        }
      });
    </script>
  </body>
</html>"##;

#[tokio::main]
async fn main() -> papero::Result<()> {
    let index = Handler::new(move |_req, stream| {
        Box::pin(async move {
            stream.send(stream_responses::Html::new(INDEX_HTML)).await?;
            Ok(())
        })
    });

    let websocket = Handler::new(move |req, stream| {
        Box::pin(async move {
            let mut ws_stream: WebSocketStream = stream.convert_into(req).await?;

            while let Some(msg) = ws_stream.next().await {
                if let Ok(Message::Text(msg)) = msg {
                    let mut hasher = DefaultHasher::new();
                    msg.hash(&mut hasher);
                    let hashed = hasher.finish();

                    ws_stream
                        .send(Message::text(format!("{msg}: {hashed}")))
                        .await
                        .expect("failed to send message");
                }
            }

            Ok(())
        })
    });

    let config = Config::new().with_http_listen_addr("127.0.0.1:8080");

    let mut router = Router::new();
    router.get("/", index);
    router.get("/ws", websocket);

    let server = Server::new(config, router);

    println!("Server started at http://localhost:8080");
    server.run().await
}
