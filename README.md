<div align="center">
  <img src="https://gitlab.com/encre-org/papero/-/raw/main/.assets/logo.svg" />
  <h1>papero</h1>
  <p>A modern, stream-based server written in Rust</p>

  <a href="https://gitlab.com/encre-org/papero/blob/main/LICENSE">
    <img alt="MIT License" src="https://img.shields.io/badge/license-MIT-success" />
  </a>

  <a href="https://deps.rs/repo/gitlab/encre-org/papero">
    <img alt="Dependency status" src="https://deps.rs/repo/gitlab/encre-org/papero/status.svg" />
  </a>

  <br>

  <a href="https://gitlab.com/encre-org/papero">
    <img alt="Number of files" src="https://tokei.ekzhang.com/b1/gitlab/encre-org/papero?category=files" />
  </a>

  <a href="https://gitlab.com/encre-org/papero">
    <img alt="Number of lines of code" src="https://tokei.ekzhang.com/b1/gitlab/encre-org/papero?category=code" />
  </a>

  <a href="https://gitlab.com/encre-org/papero">
    <img alt="Number of lines of comments" src="https://tokei.ekzhang.com/b1/gitlab/encre-org/papero?category=comments" />
  </a>

  <a href="https://gitlab.com/encre-org/papero">
    <img alt="Total number of lines" src="https://tokei.ekzhang.com/b1/gitlab/encre-org/papero" />
  </a>
</div>

### Features

- TLS
- HTTP/1 (and [Keep-Alive](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Keep-Alive))
- HTTP/2 (and [Prior-Knowledge](https://httpwg.org/specs/rfc7540.html#known-http))
- HTTP/3 (experimental)
- Routing (very fast because of [matchit](https://docs.rs/matchit))
- [WebSocket](https://developer.mozilla.org/en-US/docs/Glossary/WebSockets)
- [Server-Sent Events](https://developer.mozilla.org/en-US/docs/Web/API/Server-sent_events)
- testing `papero` servers using a simple test framework

### Design

In `papero`, everything is a stream. You start by registering a route in the router
with an handler taking a request and a stream as arguments. The request will be
automatically parsed from the stream but how you then use the stream is up to you.

Some APIs are useful to manipulate the stream:

- Using the `SendToStream` trait, you can serialize arbitrary structures and easily
  send them to the stream
- Using the `ConvertStream` trait, you can wrap a stream in an arbitrary structure
  to implement a protocol (e.g WebSocket)

Finally, structures in charge of retrieving various data from the request are called
_extractors_ and let you easily get query parameters, form data, multipart data
or JSON.

### Getting started

Have a look at [the examples](https://gitlab.com/encre-org/papero/tree/main/examples).

When adding `papero` to your `Cargo.toml`, make sure to enable the features you need
(`http2`, `http3`, `tls`, `openapi`, and extractors).

For internal reasons, you need to `Box::pin` the asynchronous future returned by every route
handler and it must return a `Result<(), papero::Error>`.

That being said, you can then get started with:

```rust
use papero::{Config, Handler, Router, Server, http::Response};

#[tokio::main]
async fn main() -> papero::Result<()> {
    let index = Handler::new(move |_req, stream| {
        Box::pin(async move {
            stream
                .send(Response::builder().body("Hello world!").unwrap())
                .await?;
            Ok(())
        })
    });

    let config = Config::new().with_http_listen_addr("127.0.0.1:8080");

    let mut router = Router::new();
    router.get("/", index);

    let server = Server::new(config, router);

    println!("Server started at http://localhost:8080");
    server.run().await
}
```
