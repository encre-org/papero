pub mod config;
pub mod error;
pub mod router;
pub mod server;
pub mod sse;
pub mod stream;
pub mod stream_responses;
pub mod utils;
pub mod ws;

pub use config::{Config, TlsConfig};
pub use error::{Error, Result};
pub use router::{Handler, Router};
pub use server::{
    http_impl::{Request, Response},
    Server,
};
pub use stream::Stream;

pub use http;

#[cfg(test)]
pub mod testing;

pub mod extract {
    #[cfg(feature = "extract-urlencoded")]
    pub use serde_urlencoded;

    #[cfg(feature = "extract-json")]
    pub use serde_json;

    #[cfg(feature = "extract-multipart")]
    pub use multer;

    use crate::server::http_impl::Request;

    // TODO: Add extractor functions taking ownership of the request's body

    /// Get URL parameters from a request.
    pub fn extract_params(req: &Request) -> &crate::router::Params {
        req.extensions()
            .get::<crate::router::Params>()
            .expect("request without params extenstion")
    }

    /// Extract an [x-www-form-urlencoded](https://url.spec.whatwg.org/#application/x-www-form-urlencoded) request query parameter present in the URL.
    #[cfg(feature = "extract-urlencoded")]
    pub fn extract_url_query<T: for<'a> serde::Deserialize<'a>>(req: &Request) -> Result<T, crate::error::ExtractError> {
        let query = req.uri().query().ok_or(crate::error::ExtractError::NoQueryParameter)?;
        serde_urlencoded::from_bytes(query.as_bytes()).map_err(crate::error::ExtractError::UrlDeserialize)
    }

    /// Extract an [x-www-form-urlencoded](https://url.spec.whatwg.org/#application/x-www-form-urlencoded) request body.
    #[cfg(feature = "extract-urlencoded")]
    pub fn extract_urlencoded_body<T: for<'a> serde::Deserialize<'a>>(req: &Request) -> Result<T, crate::error::ExtractError> {
        serde_urlencoded::from_bytes(req.body()).map_err(crate::error::ExtractError::UrlDeserialize)
    }

    /// Extract a JSON-encoded request body.
    #[cfg(feature = "extract-json")]
    pub fn extract_json<T: for<'a> serde::Deserialize<'a>>(req: &Request) -> Result<T, crate::error::ExtractError> {
        serde_json::from_slice(req.body()).map_err(crate::error::ExtractError::JsonDeserialize)
    }

    /// Extract a [multipart/form-data](https://www.rfc-editor.org/rfc/rfc7578)-encoded request body.
    #[cfg(feature = "extract-multipart")]
    pub fn extract_multipart(
        req: &Request,
        constraints: Option<multer::Constraints>,
    ) -> Result<multer::Multipart, crate::error::ExtractError> {
        use bytes::Bytes;
        use http::header;

        let content_type = req
            .headers()
            .get(header::CONTENT_TYPE)
            .ok_or_else(|| {
                crate::error::ExtractError::BadContentTypeHeader(
                    "the content type header must be present to decode multipart data",
                )
            })?
            .to_str()
            .map_err(|_| {
                crate::error::ExtractError::BadContentTypeHeader(
                    "the content type header must be a valid string",
                )
            })?;
        let boundary_keyword_pos = content_type.find("boundary=").ok_or_else(|| {
            crate::error::ExtractError::BadContentTypeHeader(
                "the content type header must contain a `boudary` key",
            )
        })?;

        Ok(multer::Multipart::with_constraints(
            futures_util::stream::once(async move {
                // TODO: Avoid cloning
                std::result::Result::<Bytes, std::convert::Infallible>::Ok(Bytes::from(req.body().to_vec()))
            }),
            &content_type[boundary_keyword_pos + 9..],
            constraints.unwrap_or_default(),
        ))
    }
}

// TODO(server): Support Transfer-Encoding headers and chunked data (e.g Range header)
// TODO(http): maybe handle trailers
// TODO: Add a way to limit requests coming from one IP

#[cfg(test)]
mod tests {
    use http::header;
    use serde::Deserialize;

    use super::*;

    #[tokio::test]
    async fn basic() {
        let mut router = Router::new();
        router.get(
            "/",
            Handler::new(|_req, stream| {
                Box::pin(async move {
                    let http_res = http::Response::builder().body("hello").unwrap();
                    stream.send(http_res).await?;
                    Ok(())
                })
            }),
        );

        let req = http::Request::builder().body("hello").unwrap();
        let res = testing::send(&router, req).await.unwrap();
        assert_eq!(std::str::from_utf8(res.body()).unwrap(), "hello");
        assert_eq!(res.headers().get(header::SERVER).unwrap(), "papero");
    }

    #[tokio::test]
    async fn route_not_found() {
        let mut router = Router::new();
        router.get(
            "/",
            Handler::new(|_req, stream| {
                Box::pin(async move {
                    let http_res = http::Response::builder().body("hello").unwrap();
                    stream.send(http_res).await?;
                    Ok(())
                })
            }),
        );
        router.not_found(Handler::new(|_req, stream| {
            Box::pin(async move {
                let http_res = http::Response::builder().body("Route not found!").unwrap();
                stream.send(http_res).await?;
                Ok(())
            })
        }));

        let req = http::Request::builder()
            .uri("/route/not/existing")
            .body("hello")
            .unwrap();
        let res = testing::send(&router, req).await.unwrap();
        assert_eq!(std::str::from_utf8(res.body()).unwrap(), "Route not found!");

        let req = http::Request::builder().uri("/").body("hello").unwrap();
        let res = testing::send(&router, req).await.unwrap();
        assert_eq!(std::str::from_utf8(res.body()).unwrap(), "hello");
    }

    #[tokio::test]
    async fn extract_params_from_request() {
        let mut router = Router::new();
        router.get(
            "/user/{user}",
            Handler::new(|req, stream| {
                Box::pin(async move {
                    let params = crate::extract::extract_params(&req);
                    let user = params.get("user").unwrap();

                    let http_res = http::Response::builder()
                        .body(format!("Hello {user}!"))
                        .unwrap();
                    stream.send(http_res).await?;
                    Ok(())
                })
            }),
        );

        let req = http::Request::builder()
            .uri("/user/Bob")
            .body("hello")
            .unwrap();
        let res = testing::send(&router, req).await.unwrap();
        assert_eq!(std::str::from_utf8(res.body()).unwrap(), "Hello Bob!");
    }

    #[tokio::test]
    async fn many_headers() {
        let mut router = Router::new();
        router.get(
            "/",
            Handler::new(|_req, stream| {
                Box::pin(async move {
                    let mut http_res = http::Response::builder();

                    // Date and Server will be added (so there will be 64 headers in total)
                    for _ in 0..61 {
                        http_res = http_res.header(header::CONTENT_TYPE, "text/plain");
                    }

                    let http_res = http_res.body("hello").unwrap();
                    stream.send(http_res).await?;
                    Ok(())
                })
            }),
        );

        let req = http::Request::builder().body("hello").unwrap();
        let res = testing::send(&router, req).await.unwrap();
        assert_eq!(std::str::from_utf8(res.body()).unwrap(), "hello");
    }

    #[tokio::test]
    async fn extract_urlencoded() {
        let mut router = Router::new();
        router.get(
            "/",
            Handler::new(|req, stream| {
                Box::pin(async move {
                    #[derive(Debug, Deserialize, PartialEq)]
                    struct Fields {
                        username: String,
                        password: String,
                        age: Option<usize>,
                    }

                    #[derive(Debug, Deserialize, PartialEq)]
                    struct QueryParams {
                        theme: String,
                    }

                    let http_res = http::Response::builder().body("hello").unwrap();
                    let query_params: QueryParams =
                        crate::extract::extract_url_query(&req).unwrap();
                    let fields: Fields = crate::extract::extract_urlencoded_body(&req).unwrap();

                    assert_eq!(
                        query_params,
                        QueryParams {
                            theme: String::from("dark"),
                        }
                    );

                    assert_eq!(
                        fields,
                        Fields {
                            username: String::from("john doe"),
                            password: String::from("123456"),
                            age: None,
                        }
                    );

                    stream.send(http_res).await?;
                    Ok(())
                })
            }),
        );

        let req = http::Request::builder()
            .uri("/?theme=dark")
            .body("username=john%20doe&password=123456")
            .unwrap();
        let res = testing::send(&router, req).await.unwrap();
        assert_eq!(std::str::from_utf8(res.body()).unwrap(), "hello");
    }

    #[tokio::test]
    async fn extract_multipart() {
        let router = Router::new().with_post(
            "/",
            Handler::new(|req, stream| {
                Box::pin(async move {
                    let http_res = http::Response::builder().body("hello").unwrap();
                    let mut multipart_data: crate::extract::multer::Multipart =
                        crate::extract::extract_multipart(&req, None).unwrap();

                    let entry_1 = multipart_data.next_field().await.unwrap().unwrap();
                    assert_eq!(entry_1.name(), Some("text"));
                    assert_eq!(entry_1.text().await, Ok("text default".to_string()));

                    let entry_2 = multipart_data.next_field().await.unwrap().unwrap();
                    assert_eq!(entry_2.name(), Some("file1"));
                    assert_eq!(entry_2.file_name(), Some("a.txt"));
                    assert_eq!(
                        entry_2.headers().get(header::CONTENT_TYPE),
                        Some(&http::HeaderValue::from_str("text/plain").unwrap())
                    );
                    assert_eq!(
                        entry_2.text().await,
                        Ok("Content of a.txt.\r\n".to_string())
                    );

                    let entry_3 = multipart_data.next_field().await.unwrap().unwrap();
                    assert_eq!(entry_3.name(), Some("file2"));
                    assert_eq!(entry_3.file_name(), Some("a.html"));
                    assert_eq!(
                        entry_3.headers().get(header::CONTENT_TYPE),
                        Some(&http::HeaderValue::from_str("text/html").unwrap())
                    );
                    assert_eq!(
                        entry_3.text().await,
                        Ok("<!DOCTYPE html><title>Content of a.html.</title>\r\n".to_string())
                    );

                    stream.send(http_res).await?;
                    Ok(())
                })
            }),
        );

        let req = http::Request::builder()
            .method(http::Method::POST)
            .header(header::CONTENT_TYPE, "multipart/form-data; boundary=---------------------------9051914041544843365972754266")
            .body(
                r#"-----------------------------9051914041544843365972754266
Content-Disposition: form-data; name="text"

text default
-----------------------------9051914041544843365972754266
Content-Disposition: form-data; name="file1"; filename="a.txt"
Content-Type: text/plain

Content of a.txt.

-----------------------------9051914041544843365972754266
Content-Disposition: form-data; name="file2"; filename="a.html"
Content-Type: text/html

<!DOCTYPE html><title>Content of a.html.</title>

-----------------------------9051914041544843365972754266--"#.replace('\n', "\r\n"),
            )
            .unwrap();
        let res = testing::send(&router, req).await.unwrap();
        assert_eq!(std::str::from_utf8(res.body()).unwrap(), "hello");
    }

    #[tokio::test]
    async fn extract_json() {
        let mut router = Router::new();
        router.get(
            "/",
            Handler::new(|req, stream| {
                Box::pin(async move {
                    #[derive(Debug, Deserialize, PartialEq)]
                    struct Fields {
                        username: String,
                        password: String,
                        age: Option<usize>,
                    }

                    let http_res = http::Response::builder().body("hello").unwrap();
                    let fields: Fields = crate::extract::extract_json(&req).unwrap();

                    assert_eq!(
                        fields,
                        Fields {
                            username: String::from("john doe"),
                            password: String::from("123456"),
                            age: None,
                        }
                    );

                    stream.send(http_res).await?;
                    Ok(())
                })
            }),
        );

        let req = http::Request::builder()
            .body(r#"{ "username": "john doe", "password": "123456" }"#)
            .unwrap();
        let res = testing::send(&router, req).await.unwrap();
        assert_eq!(std::str::from_utf8(res.body()).unwrap(), "hello");
    }
}
