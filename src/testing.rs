use futures::Future;

use http::{Request, Response};
use tokio::{io::BufStream, net::TcpStream};
use tokio_util::sync::CancellationToken;

use crate::{
    server::{
        http_impl::{HttpImpl, HTTP1},
        tls::MaybeTlsStream,
    },
    Router, Server, Stream,
};

pub(crate) struct TestStream {
    pub(crate) client: MaybeTlsStream,
    pub(crate) server: MaybeTlsStream,
}

pub fn assert_requests_equality<T: AsRef<[u8]>>(req1: Request<T>, req2: Request<T>) {
    assert_eq!(req1.uri(), req2.uri());
    assert_eq!(req1.version(), req2.version());
    assert_eq!(req1.method(), req2.method());
    assert_eq!(req1.headers(), req2.headers());
    assert_eq!(req1.body().as_ref(), req2.body().as_ref());
}

pub fn assert_responses_equality<T: AsRef<[u8]>>(res1: Response<T>, res2: Response<T>) {
    assert_eq!(res1.version(), res2.version());
    assert_eq!(res1.status(), res2.status());
    assert_eq!(res1.headers(), res2.headers());
    assert_eq!(res1.body().as_ref(), res2.body().as_ref());
}

pub(crate) async fn test_stream<
    R,
    T: Future<Output = crate::Result<R>>,
    F: FnOnce(TestStream) -> T,
>(
    f: F,
) -> crate::Result<R> {
    // TODO: Maybe create only one listener per run and reset it at the end of the function
    let listener = std::net::TcpListener::bind("127.0.0.1:0").unwrap();

    let client_stream = TcpStream::connect(listener.local_addr().unwrap()).await?;
    let server_stream = listener.accept().unwrap();

    f(TestStream {
        client: MaybeTlsStream::WithoutTls(BufStream::new(client_stream)),
        server: MaybeTlsStream::WithoutTls(BufStream::new(
            TcpStream::from_std(server_stream.0.try_clone().unwrap()).unwrap(),
        )),
    })
    .await
}

pub async fn send<T: AsRef<[u8]>>(
    router: &Router,
    req: Request<T>,
) -> crate::Result<Response<Vec<u8>>> {
    let req = req.map(|b| b.as_ref().to_vec());
    test_stream(|stream| async move {
        // TODO: Test all HTTP versions
        let mut server_stream = Stream::from_http(
            HttpImpl::HTTP1(HTTP1::from_stream(stream.server)),
            CancellationToken::new(),
        );
        Server::handle_request(router, &mut server_stream, req).await?;

        let mut http_impl_client = HTTP1::from_stream(stream.client);
        let res = http_impl_client.parse_response().await.unwrap().unwrap();
        Ok(res)
    })
    .await
}
