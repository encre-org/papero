//! Define a bunch of common response helpers.
//!
//! For more complex responses, you may need to send a [`Response`] directly or any structure
//! implementing the [`SendToStream`] trait.
//!
//! [`Response`]: http::Response
//! [`SendToStream`]: crate::stream::SendToStream

macro_rules! def_stream_response {
    ($struct_name: tt, $content_type: literal) => {
        pub struct $struct_name(std::borrow::Cow<'static, str>);

        impl $struct_name {
            pub fn new<T: Into<std::borrow::Cow<'static, str>>>(contents: T) -> Self {
                Self(contents.into())
            }
        }

        #[async_trait::async_trait]
        impl crate::stream::SendToStream for $struct_name {
            async fn send_to_stream(
                self,
                stream: &mut crate::stream::Stream,
            ) -> crate::Result<()> {
                stream
                    .send(
                        http::Response::builder()
                            .header(http::header::CONTENT_TYPE, $content_type)
                            .body(self.0.as_ref())
                            .expect("failed to build response"),
                    )
                    .await
            }
        }
    };
}

def_stream_response!(Html, "text/html");
def_stream_response!(Css, "text/css");
def_stream_response!(JavaScript, "text/javascript");
def_stream_response!(Json, "application/json");
def_stream_response!(Text, "text/plain");
