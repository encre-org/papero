use crate::{
    server::{
        http_impl::{HttpImpl, Request, Response},
        tls::MaybeTlsStream,
    },
    stream::ConvertStream,
    Stream,
};
use http::Version;
use tokio_tungstenite::tungstenite::protocol::Role;

pub use futures_util::{SinkExt, StreamExt};
pub use tokio_tungstenite::tungstenite::Message;

pub type WebSocketStream = tokio_tungstenite::WebSocketStream<MaybeTlsStream>;

#[async_trait::async_trait]
impl ConvertStream for WebSocketStream {
    async fn convert_stream(stream: &mut Stream, req: Request) -> crate::Result<WebSocketStream> {
        if stream.http_version() == Version::HTTP_11 {
            let res = tokio_tungstenite::tungstenite::handshake::server::create_response(
                &http::Request::from_parts(req.into_parts().0, ()),
            )
            .map(|res| Response::from_parts(res.into_parts().0, vec![]))
            .expect("failed to make Websocket response from request"); // TODO: Error management
            stream.send(res).await?;

            #[allow(irrefutable_let_patterns)]
            let HttpImpl::HTTP1(http_impl) = stream.into_http() else {
                unreachable!("HTTP version was already checked before");
            };

            Ok(WebSocketStream::from_raw_socket(http_impl.into_stream(), Role::Server, None).await)
        } else {
            unimplemented!("WebSockets are not yet implemented when not using HTTP/1");
        }
    }
}
