pub type Result<T> = std::result::Result<T, Error>;

#[derive(thiserror::Error, Debug)]
pub enum Error {
    #[error("io error: `{0}`")]
    IO(#[from] std::io::Error),

    #[error(transparent)]
    HttpImpl(#[from] HttpImplError),

    #[error(transparent)]
    Server(#[from] ServerError),

    #[error(transparent)]
    Extract(#[from] ExtractError),

    #[error("{0}")]
    Other(String),
}

impl Error {
    pub fn other<T: Into<String>>(s: T) -> Self {
        Self::Other(s.into())
    }
}

#[derive(thiserror::Error, Debug)]
pub enum HttpImplError {
    #[error("io error: `{0}`")]
    IO(#[from] std::io::Error),

    #[error("HTTP/1 error: `{0}`")]
    Http1(#[from] httparse::Error),

    #[cfg(feature = "http2")]
    #[error("HTTP/2 error: `{0}`")]
    Http2(#[from] h2::Error),

    #[cfg(feature = "http3")]
    #[error("HTTP/3 error: `{0}`")]
    Http3(#[from] h3::Error),

    #[cfg(feature = "http2")]
    #[error("the request used HTTP/2 although the HTTP/1 parser was used")]
    SwitchToHttp2(Vec<u8>),

    #[error("failed to convert header value to string: `{0}`")]
    HeaderValueToString(http::header::ToStrError),

    #[error("the server API can't be mixed with the client API, eg it is not possible to call `parse_request` then `send_request` on the same connection")]
    MixedClientServer,

    #[error("parsing a request using `parse_request` is required in order to send a response with `send_response`")]
    RequestRequiredForResponse,

    #[error("read data is too large for the buffer, you may need to increase it using `MAX_BUFFER_SIZE`, {0} bytes needed")]
    RequestParsingBufferTooSmall(usize),

    #[error("malformed request data, check the header values and the body contents")]
    MalformedRequestData,

    #[error("malformed response data, check the header values and the body contents")]
    MalformedResponseData,
}

#[derive(thiserror::Error, Debug)]
pub enum ExtractError {
    #[cfg(feature = "extract-urlencoded")]
    #[error("no query parameter present in the URI")]
    NoQueryParameter,

    #[cfg(feature = "extract-urlencoded")]
    #[error("urlencoded deserializing error: `{0}`")]
    UrlDeserialize(serde_urlencoded::de::Error),

    #[cfg(feature = "extract-json")]
    #[error("JSON deserializing error: `{0}`")]
    JsonDeserialize(serde_json::Error),

    #[cfg(feature = "extract-multipart")]
    #[error("bad `Content-Type` header for parsing multipart-encoded request: `{0}`")]
    BadContentTypeHeader(&'static str),
}

#[derive(thiserror::Error, Debug)]
pub enum ServerError {
    #[cfg(feature = "http3")]
    #[error("quic error: `{0}`")]
    Quinn(#[from] quinn::ConnectionError),

    #[cfg(feature = "http3")]
    #[error("quic server configuration error: `{0}`")]
    BadQuicServerConfig(quinn::crypto::rustls::NoInitialCipherSuite),

    #[error("no private key found in the key file")]
    NoPrivateKey,

    #[error("failed to accept the incoming connection")]
    Accept,
}
