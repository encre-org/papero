use std::{borrow::Borrow, collections::HashMap, hash::Hash};

use futures::future::BoxFuture;
use http::{Method, StatusCode};

use crate::{server::http_impl::Request, Stream};

#[cfg(feature = "openapi")]
pub mod openapi;

type HandlerFn = Box<dyn Fn(Request, &mut Stream) -> BoxFuture<crate::Result<()>> + Send + Sync>;

pub struct Handler {
    inner: HandlerFn,
}

impl Handler {
    pub fn new<
        F: for<'a> Fn(Request, &'a mut Stream) -> BoxFuture<'a, crate::Result<()>>
            + Send
            + Sync
            + 'static,
    >(
        f: F,
    ) -> Self {
        Self { inner: Box::new(f) }
    }

    pub(crate) async fn call(&self, req: Request, stream: &mut Stream) -> crate::Result<()> {
        (self.inner)(req, stream).await
    }
}

#[derive(Debug, Default, Clone, PartialEq)]
pub struct Params {
    inner: HashMap<String, String>,
}

impl Params {
    pub(crate) fn new() -> Self {
        Self {
            inner: HashMap::new(),
        }
    }

    pub fn is_empty(&self) -> bool {
        self.inner.is_empty()
    }

    pub fn len(&self) -> usize {
        self.inner.len()
    }

    pub fn contains_key<K: Hash + Eq + ?Sized>(&self, key: &K) -> bool
    where
        String: Borrow<K>,
    {
        self.inner.contains_key(key)
    }

    pub fn get<K: Hash + Eq + ?Sized>(&self, key: &K) -> Option<&String>
    where
        String: Borrow<K>,
    {
        self.inner.get(key)
    }

    /// Get an iterator over the entries of the context, sorted by key.
    pub fn iter(&self) -> impl Iterator<Item = (&String, &String)> {
        self.inner.iter()
    }
}

impl<'k, 'v> From<matchit::Params<'k, 'v>> for Params {
    fn from(val: matchit::Params) -> Self {
        Self {
            inner: HashMap::from_iter(val.iter().map(|(k, v)| (k.to_string(), v.to_string()))),
        }
    }
}

macro_rules! route_method {
    ($fn_name: tt, $with_fn_name: tt, $fn_name_openapi: tt, $method:path) => {
        pub fn $fn_name(&mut self, uri: &str, handler: Handler) {
            if let Some(router) = self.inner.get_mut(&Some($method)) {
                router
                    .insert(uri, handler)
                    .expect("failed to parse the uri inserted into the router");
            } else {
                let mut router = matchit::Router::new();
                router
                    .insert(uri, handler)
                    .expect("failed to parse the uri inserted into the router");
                self.inner.insert(Some($method), router);
            }
        }

        pub fn $with_fn_name(mut self, uri: &str, handler: Handler) -> Self {
            if let Some(router) = self.inner.get_mut(&Some($method)) {
                router
                    .insert(uri, handler)
                    .expect("failed to parse the uri inserted into the router");
            } else {
                let mut router = matchit::Router::new();
                router
                    .insert(uri, handler)
                    .expect("failed to parse the uri inserted into the router");
                self.inner.insert(Some($method), router);
            }
            self
        }

        #[cfg(feature = "openapi")]
        pub fn $fn_name_openapi(
            &mut self,
            uri: &str,
            handler: Handler,
            openapi_operation: openapi::Operation,
        ) {
            self.$fn_name(uri, handler);
            let path_item = self.openapi.as_mut().expect("you need to call `Router::with_openapi_spec` or `Router::insert_openapi_spec` before calling any `Router::*_openapi` method").insert_path(uri);
            path_item.$fn_name = Some(openapi_operation);
        }
    };
}

pub struct Router {
    inner: HashMap<Option<Method>, matchit::Router<Handler>>,
    not_found: Handler,

    #[cfg(feature = "openapi")]
    openapi: Option<openapi::OpenAPI>,
}

impl Router {
    pub fn new() -> Self {
        Self::default()
    }

    #[cfg(feature = "openapi")]
    pub fn with_openapi_spec(mut self, openapi: openapi::OpenAPI) -> Self {
        self.openapi = Some(openapi);
        self
    }

    #[cfg(feature = "openapi")]
    pub fn insert_openapi_spec(&mut self, openapi: openapi::OpenAPI) {
        self.openapi = Some(openapi);
    }

    #[cfg(feature = "openapi")]
    pub fn get_openapi_spec(&self) -> Option<&openapi::OpenAPI> {
        self.openapi.as_ref()
    }

    #[cfg(feature = "openapi")]
    pub fn get_mut_openapi_spec(&mut self) -> Option<&mut openapi::OpenAPI> {
        self.openapi.as_mut()
    }

    #[cfg(feature = "openapi")]
    pub fn take_openapi_spec(&mut self) -> Option<openapi::OpenAPI> {
        self.openapi.take()
    }

    pub(crate) fn at(&self, req: &Request) -> (&Handler, Params) {
        #[cfg(feature = "extract-urlencoded")]
        let method = {
            // TODO: Cache the result somewhere (to avoid extracting the contents twice)
            #[derive(serde::Deserialize)]
            struct Query {
                #[serde(rename = "_method")]
                method: Option<String>,
            }

            if let Some(method) = crate::extract::extract_url_query::<Query>(req)
                .ok()
                .and_then(|q| q.method)
                .and_then(|m| m.parse().ok())
            {
                method
            } else {
                req.method().clone()
            }
        };

        #[cfg(not(feature = "extract-urlencoded"))]
        let method = req.method().clone();

        match self.inner.get(&Some(method)) {
            Some(router) => match router.at(req.uri().path()).ok() {
                Some(matching) => (matching.value, Params::from(matching.params)),
                None => match self
                    .inner
                    .get(&None)
                    .and_then(|router| router.at(req.uri().path()).ok())
                {
                    Some(matching) => (matching.value, Params::from(matching.params)),
                    None => (&self.not_found, Params::new()),
                },
            },
            None => (&self.not_found, Params::new()),
        }
    }

    pub fn with_not_found(mut self, handler: Handler) -> Self {
        self.not_found = handler;
        self
    }

    pub fn not_found(&mut self, handler: Handler) {
        self.not_found = handler;
    }

    pub fn with_all(mut self, uri: &str, handler: Handler) -> Self {
        if let Some(router) = self.inner.get_mut(&None) {
            router
                .insert(uri, handler)
                .expect("failed to parse the uri inserted into the router");
        } else {
            let mut router = matchit::Router::new();
            router
                .insert(uri, handler)
                .expect("failed to parse the uri inserted into the router");
            self.inner.insert(None, router);
        }
        self
    }

    pub fn all(&mut self, uri: &str, handler: Handler) {
        if let Some(router) = self.inner.get_mut(&None) {
            router
                .insert(uri, handler)
                .expect("failed to parse the uri inserted into the router");
        } else {
            let mut router = matchit::Router::new();
            router
                .insert(uri, handler)
                .expect("failed to parse the uri inserted into the router");
            self.inner.insert(None, router);
        }
    }

    #[cfg(feature = "openapi")]
    pub fn all_openapi(
        &mut self,
        uri: &str,
        handler: Handler,
        openapi_operation: openapi::Operation,
    ) {
        self.all(uri, handler);
        let path_item = self.openapi.as_mut().expect("you need to call `Router::with_openapi_spec` or `Router::insert_openapi_spec` before calling any `Router::*_openapi` method").insert_path(uri);

        path_item.get = Some(openapi_operation.clone());
        path_item.put = Some(openapi_operation.clone());
        path_item.post = Some(openapi_operation.clone());
        path_item.delete = Some(openapi_operation.clone());
        path_item.options = Some(openapi_operation.clone());
        path_item.head = Some(openapi_operation.clone());
        path_item.patch = Some(openapi_operation.clone());
        path_item.trace = Some(openapi_operation.clone());
    }

    route_method!(get, with_get, get_openapi, Method::GET);
    route_method!(post, with_post, post_openapi, Method::POST);
    route_method!(put, with_put, put_openapi, Method::PUT);
    route_method!(delete, with_delete, delete_openapi, Method::DELETE);
    route_method!(head, with_head, head_openapi, Method::HEAD);
    route_method!(options, with_options, options_openapi, Method::OPTIONS);
    route_method!(connect, with_connect, connect_openapi, Method::CONNECT);
    route_method!(patch, with_patch, patch_openapi, Method::PATCH);
    route_method!(trace, with_trace, trace_openapi, Method::TRACE);
}

impl Default for Router {
    fn default() -> Self {
        Self {
            inner: HashMap::new(),
            not_found: Handler::new(|_req, stream| {
                Box::pin(async move {
                    stream
                        .send(
                            http::Response::builder()
                                .status(StatusCode::NOT_FOUND)
                                .body(b"404 Not Found".to_vec())
                                .unwrap(),
                        )
                        .await?;
                    Ok(())
                })
            }),

            #[cfg(feature = "openapi")]
            openapi: None,
        }
    }
}
