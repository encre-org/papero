//! OpenAPI v3.1 structures
//!
//! Specification at https://github.com/OAI/OpenAPI-Specification/blob/main/versions/3.1.0.md
use serde::{Deserialize, Serialize};
use std::{collections::HashMap, fmt::Debug};

#[derive(Debug, PartialEq, Clone, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
#[serde(untagged)]
pub enum OrReference<T> {
    Reference(Reference),
    Value(T),
}

#[derive(Debug, PartialEq, Clone, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
#[serde(untagged)]
pub enum OrExpression<T> {
    Value(T),
    Expression(String),
}

#[derive(Debug, PartialEq, Clone, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct OpenAPI {
    pub openapi: String,
    pub info: Info,
    pub json_schema_dialect: Option<String>,
    pub servers: Option<Vec<Server>>,
    pub paths: Option<Paths>,
    pub webhooks: Option<HashMap<String, OrReference<PathItem>>>,
    pub components: Option<Components>,
    pub security: Option<Vec<SecurityRequirements>>,
    pub tags: Option<Vec<Tag>>,
    pub external_docs: Option<ExternalDocumentation>,

    #[serde(flatten)]
    pub extensions: HashMap<String, serde_json::Value>,
}

impl OpenAPI {
    pub fn new(info: Info) -> Self {
        OpenAPI {
            openapi: String::from("3.1.0"),
            info,
            json_schema_dialect: None,
            servers: None,
            paths: None,
            webhooks: None,
            components: None,
            security: None,
            tags: None,
            external_docs: None,
            extensions: HashMap::new(),
        }
    }

    pub fn insert_path<S: Into<String>>(&mut self, path: S) -> &mut PathItem {
        if self.paths.is_none() {
            self.paths = Some(Paths(HashMap::new()));
        }

        let path = path.into();
        self.paths
            .as_mut()
            .unwrap()
            .0
            .insert(path.clone(), PathItem::default());
        self.paths.as_mut().unwrap().0.get_mut(&path).unwrap()
    }
}

#[derive(Debug, PartialEq, Clone, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Info {
    pub title: String,
    pub summary: Option<String>,
    pub description: Option<String>,
    pub terms_of_service: Option<String>,
    pub contact: Option<Contact>,
    pub license: Option<License>,
    pub version: String,

    #[serde(flatten)]
    pub extensions: HashMap<String, serde_json::Value>,
}

impl Info {
    pub fn new<S1: Into<String>, S2: Into<String>>(title: S1, version: S2) -> Self {
        Info {
            title: title.into(),
            summary: None,
            description: None,
            terms_of_service: None,
            contact: None,
            license: None,
            version: version.into(),
            extensions: HashMap::new(),
        }
    }

    pub fn with_summary<S: Into<String>>(mut self, summary: S) -> Self {
        self.summary = Some(summary.into());
        self
    }

    pub fn with_description<S: Into<String>>(mut self, description: S) -> Self {
        self.description = Some(description.into());
        self
    }

    pub fn with_terms_of_service<S: Into<String>>(mut self, terms_of_service: S) -> Self {
        self.terms_of_service = Some(terms_of_service.into());
        self
    }

    pub fn with_contact(mut self, contact: Contact) -> Self {
        self.contact = Some(contact);
        self
    }

    pub fn with_license(mut self, license: License) -> Self {
        self.license = Some(license);
        self
    }
}

#[derive(Debug, PartialEq, Clone, Serialize, Deserialize, Default)]
#[serde(rename_all = "camelCase")]
pub struct Contact {
    pub name: Option<String>,
    pub url: Option<String>,
    pub email: Option<String>,

    #[serde(flatten)]
    pub extensions: HashMap<String, serde_json::Value>,
}

#[derive(Debug, PartialEq, Clone, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct License {
    pub name: String,
    pub identifier: Option<String>,
    pub url: Option<String>,

    #[serde(flatten)]
    pub extensions: HashMap<String, serde_json::Value>,
}

impl License {
    pub fn new<S: Into<String>>(name: S) -> Self {
        Self {
            name: name.into(),
            identifier: None,
            url: None,
            extensions: HashMap::new(),
        }
    }
}

#[derive(Debug, PartialEq, Clone, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Server {
    pub url: String,
    pub description: Option<String>,
    pub variables: Option<HashMap<String, ServerVariable>>,

    #[serde(flatten)]
    pub extensions: HashMap<String, serde_json::Value>,
}

impl Server {
    pub fn new<S: Into<String>>(url: S) -> Self {
        Self {
            url: url.into(),
            description: None,
            variables: None,
            extensions: HashMap::new(),
        }
    }
}

#[derive(Debug, PartialEq, Clone, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct ServerVariable {
    #[serde(rename = "enum")]
    pub enum_: Option<Vec<String>>,
    pub default: String,
    pub description: Option<String>,

    #[serde(flatten)]
    pub extensions: HashMap<String, serde_json::Value>,
}

impl ServerVariable {
    pub fn new<S: Into<String>>(default: S) -> Self {
        Self {
            enum_: None,
            default: default.into(),
            description: None,
            extensions: HashMap::new(),
        }
    }
}

#[derive(Debug, PartialEq, Clone, Serialize, Deserialize, Default)]
#[serde(rename_all = "camelCase")]
pub struct Components {
    pub schemas: Option<HashMap<String, Schema>>,
    pub responses: Option<HashMap<String, OrReference<Response>>>,
    pub parameters: Option<HashMap<String, OrReference<Parameter>>>,
    pub examples: Option<HashMap<String, OrReference<Example>>>,
    pub request_bodies: Option<HashMap<String, OrReference<RequestBody>>>,
    pub headers: Option<HashMap<String, OrReference<Header>>>,
    pub security_schemes: Option<HashMap<String, OrReference<SecurityScheme>>>,
    pub links: Option<HashMap<String, OrReference<Link>>>,
    pub callbacks: Option<HashMap<String, OrReference<Callback>>>,
    pub path_items: Option<HashMap<String, OrReference<PathItem>>>,

    #[serde(flatten)]
    pub extensions: HashMap<String, serde_json::Value>,
}

// TODO: This structure does not support specification extensions
#[derive(Debug, PartialEq, Clone, Serialize, Deserialize, Default)]
#[repr(transparent)]
#[serde(rename_all = "camelCase")]
pub struct Paths(pub HashMap<String, PathItem>);

#[derive(Debug, PartialEq, Clone, Serialize, Deserialize, Default)]
#[serde(rename_all = "camelCase")]
pub struct PathItem {
    #[serde(rename = "$ref")]
    pub ref_: Option<String>,
    pub summary: Option<String>,
    pub description: Option<String>,
    pub get: Option<Operation>,
    pub put: Option<Operation>,
    pub post: Option<Operation>,
    pub delete: Option<Operation>,
    pub options: Option<Operation>,
    pub head: Option<Operation>,
    pub patch: Option<Operation>,
    pub trace: Option<Operation>,

    // NOT in the v3.1.0 specification but is considered to be included
    // in next versions (https://github.com/OAI/OpenAPI-Specification/issues/1747)
    // Needed for the router to work
    pub connect: Option<Operation>,

    pub servers: Option<Vec<Server>>,
    pub parameters: Option<Vec<OrReference<Parameter>>>,

    #[serde(flatten)]
    pub extensions: HashMap<String, serde_json::Value>,
}

#[derive(Debug, PartialEq, Clone, Serialize, Deserialize, Default)]
#[serde(rename_all = "camelCase")]
pub struct Operation {
    pub tags: Option<Vec<String>>,
    pub summary: Option<String>,
    pub description: Option<String>,
    pub external_docs: Option<ExternalDocumentation>,
    pub operation_id: Option<String>,
    pub parameters: Option<Vec<OrReference<Parameter>>>,
    pub request_body: Option<OrReference<RequestBody>>,
    pub responses: Option<Responses>,
    pub callbacks: Option<HashMap<String, OrReference<Callback>>>,
    pub deprecated: Option<bool>,
    pub security: Option<SecurityRequirements>,
    pub servers: Option<Vec<Server>>,

    #[serde(flatten)]
    pub extensions: HashMap<String, serde_json::Value>,
}

impl Operation {
    pub fn with_tag<S: Into<String>>(mut self, tag: S) -> Self {
        if self.tags.is_none() {
            self.tags = Some(vec![]);
        }

        self.tags.as_mut().unwrap().push(tag.into());

        self
    }

    pub fn with_tags<S: Into<String>>(mut self, tags: impl IntoIterator<Item = S>) -> Self {
        if self.tags.is_none() {
            self.tags = Some(vec![]);
        }

        self.tags
            .as_mut()
            .unwrap()
            .extend(tags.into_iter().map(|t| t.into()));

        self
    }

    pub fn with_summary<S: Into<String>>(mut self, summary: S) -> Self {
        self.summary = Some(summary.into());
        self
    }

    pub fn with_description<S: Into<String>>(mut self, description: S) -> Self {
        self.description = Some(description.into());
        self
    }

    pub fn with_external_docs(mut self, external_docs: ExternalDocumentation) -> Self {
        self.external_docs = Some(external_docs);
        self
    }

    pub fn with_operation_id<S: Into<String>>(mut self, operation_id: S) -> Self {
        self.operation_id = Some(operation_id.into());
        self
    }

    pub fn with_parameter(mut self, parameter: OrReference<Parameter>) -> Self {
        if self.parameters.is_none() {
            self.parameters = Some(vec![]);
        }

        self.parameters.as_mut().unwrap().push(parameter);

        self
    }

    pub fn with_parameters(
        mut self,
        parameters: impl IntoIterator<Item = OrReference<Parameter>>,
    ) -> Self {
        if self.parameters.is_none() {
            self.parameters = Some(vec![]);
        }

        self.parameters.as_mut().unwrap().extend(parameters);

        self
    }

    pub fn with_request_body(mut self, request_body: OrReference<RequestBody>) -> Self {
        self.request_body = Some(request_body);
        self
    }

    pub fn with_default_response(mut self, default_response: OrReference<Response>) -> Self {
        if self.responses.is_none() {
            self.responses = Some(Responses::default());
        }

        self.responses.as_mut().unwrap().default = Some(default_response);

        self
    }

    pub fn with_response(
        mut self,
        code: http::StatusCode,
        response: OrReference<Response>,
    ) -> Self {
        if self.responses.is_none() {
            self.responses = Some(Responses::default());
        }

        self.responses
            .as_mut()
            .unwrap()
            .codes
            .insert(u16::from(code).to_string(), response);

        self
    }

    pub fn with_responses(
        mut self,
        responses: impl IntoIterator<Item = (http::StatusCode, OrReference<Response>)>,
    ) -> Self {
        if self.responses.is_none() {
            self.responses = Some(Responses::default());
        }

        self.responses.as_mut().unwrap().codes.extend(
            responses
                .into_iter()
                .map(|(code, r)| (u16::from(code).to_string(), r)),
        );

        self
    }

    pub fn with_callback<S: Into<String>>(mut self, key: S, callback: OrReference<Callback>) -> Self {
        if self.callbacks.is_none() {
            self.callbacks = Some(HashMap::new());
        }

        self.callbacks.as_mut().unwrap().insert(key.into(), callback);

        self
    }

    pub fn with_callbacks<S: Into<String>>(
        mut self,
        callbacks: impl IntoIterator<Item = (S, OrReference<Callback>)>,
    ) -> Self {
        if self.callbacks.is_none() {
            self.callbacks = Some(HashMap::new());
        }

        self.callbacks.as_mut().unwrap().extend(callbacks.into_iter().map(|(k, v)| (k.into(), v)));

        self
    }

    pub fn with_deprecated(mut self) -> Self {
        self.deprecated =  Some(true);
        self
    }

    pub fn with_security<S: Into<String>>(mut self, key: S, security: Vec<String>) -> Self {
        if self.security.is_none() {
            self.security = Some(SecurityRequirements(HashMap::new()));
        }

        self.security.as_mut().unwrap().0.insert(key.into(), security);

        self
    }

    pub fn with_securitys<S: Into<String>>(
        mut self,
        securitys: impl IntoIterator<Item = (S, Vec<String>)>,
    ) -> Self {
        if self.security.is_none() {
            self.security = Some(SecurityRequirements(HashMap::new()));
        }

        self.security.as_mut().unwrap().0.extend(securitys.into_iter().map(|(k, v)| (k.into(), v)));

        self
    }

    pub fn with_server(mut self, server: Server) -> Self {
        if self.servers.is_none() {
            self.servers = Some(vec![]);
        }

        self.servers.as_mut().unwrap().push(server);

        self
    }

    pub fn with_servers<S: Into<String>>(
        mut self,
        servers: impl IntoIterator<Item = Server>,
    ) -> Self {
        if self.servers.is_none() {
            self.servers = Some(vec![]);
        }

        self.servers.as_mut().unwrap().extend(servers);

        self
    }
}

#[derive(Debug, PartialEq, Clone, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct ExternalDocumentation {
    pub description: Option<String>,
    pub url: String,

    #[serde(flatten)]
    pub extensions: HashMap<String, serde_json::Value>,
}

impl ExternalDocumentation {
    pub fn new<S: Into<String>>(url: S) -> Self {
        Self {
            description: None,
            url: url.into(),
            extensions: HashMap::new(),
        }
    }
}

#[derive(Debug, PartialEq, Clone, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
#[serde(untagged)]
pub enum ParameterRule {
    SchemaStyle {
        style: Option<String>,
        explode: Option<bool>,
        allow_reserved: Option<bool>,
        schema: Schema,
        example: Option<serde_json::Value>,
        examples: Option<HashMap<String, OrReference<Example>>>,
    },
    Content {
        content: HashMap<String, MediaType>,
    },
}

#[derive(Debug, PartialEq, Clone, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Parameter {
    pub name: String,

    #[serde(rename = "in")]
    pub in_: String,
    pub description: Option<String>,
    pub required: Option<bool>,
    pub deprecated: Option<bool>,
    pub allow_empty_value: Option<bool>,

    #[serde(flatten)]
    pub parameter_rule: ParameterRule,

    #[serde(flatten)]
    pub extensions: HashMap<String, serde_json::Value>,
}

#[derive(Debug, PartialEq, Clone, Serialize, Deserialize, Default)]
#[serde(rename_all = "camelCase")]
pub struct RequestBody {
    pub description: Option<String>,
    pub content: HashMap<String, MediaType>,
    pub required: Option<bool>,

    #[serde(flatten)]
    pub extensions: HashMap<String, serde_json::Value>,
}

#[derive(Debug, PartialEq, Clone, Serialize, Deserialize, Default)]
#[serde(rename_all = "camelCase")]
pub struct MediaType {
    pub schema: Option<Schema>,
    pub example: Option<serde_json::Value>,
    pub examples: Option<HashMap<String, OrReference<Example>>>,
    pub encoding: Option<HashMap<String, Encoding>>,

    #[serde(flatten)]
    pub extensions: HashMap<String, serde_json::Value>,
}

impl MediaType {
    pub fn with_schema(mut self, schema: Schema) -> Self {
        self.schema = Some(schema);
        self
    }

    pub fn with_single_example(mut self, example: serde_json::Value) -> Self {
        self.example = Some(example);
        self
    }

    pub fn with_example<S: Into<String>>(mut self, key: S, example: OrReference<Example>) -> Self {
        if self.examples.is_none() {
            self.examples = Some(HashMap::new());
        }

        self.examples.as_mut().unwrap().insert(key.into(), example);

        self
    }

    pub fn with_examples<S: Into<String>>(
        mut self,
        examples: impl IntoIterator<Item = (S, OrReference<Example>)>,
    ) -> Self {
        if self.examples.is_none() {
            self.examples = Some(HashMap::new());
        }

        self.examples.as_mut().unwrap().extend(examples.into_iter().map(|(k, v)| (k.into(), v)));

        self
    }

    pub fn with_encoding<S: Into<String>>(mut self, key: S, encoding: Encoding) -> Self {
        if self.encoding.is_none() {
            self.encoding = Some(HashMap::new());
        }

        self.encoding.as_mut().unwrap().insert(key.into(), encoding);

        self
    }

    pub fn with_encodings<S: Into<String>>(
        mut self,
        encodings: impl IntoIterator<Item = (S, Encoding)>,
    ) -> Self {
        if self.encoding.is_none() {
            self.encoding = Some(HashMap::new());
        }

        self.encoding.as_mut().unwrap().extend(encodings.into_iter().map(|(k, v)| (k.into(), v)));

        self
    }
}

#[derive(Debug, PartialEq, Clone, Serialize, Deserialize, Default)]
#[serde(rename_all = "camelCase")]
pub struct Encoding {
    pub content_type: Option<String>,
    pub headers: Option<HashMap<String, OrReference<Header>>>,
    pub style: Option<String>,
    pub explode: Option<bool>,
    pub allow_reserved: Option<bool>,

    #[serde(flatten)]
    pub extensions: HashMap<String, serde_json::Value>,
}

#[derive(Debug, PartialEq, Clone, Serialize, Deserialize, Default)]
#[serde(rename_all = "camelCase")]
pub struct Responses {
    pub default: Option<OrReference<Response>>,

    #[serde(flatten)]
    pub codes: HashMap<String, OrReference<Response>>,

    #[serde(flatten)]
    pub extensions: HashMap<String, serde_json::Value>,
}

#[derive(Debug, PartialEq, Clone, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Response {
    pub description: String,
    pub headers: Option<HashMap<String, OrReference<Header>>>,
    pub content: Option<HashMap<String, MediaType>>,
    pub links: Option<HashMap<String, OrReference<Link>>>,

    #[serde(flatten)]
    pub extensions: HashMap<String, serde_json::Value>,
}

impl Response {
    pub fn new<S: Into<String>>(description: S) -> Self {
        Self {
            description: description.into(),
            headers: None,
            content: None,
            links: None,
            extensions: HashMap::new(),
        }
    }

    pub fn with_header<S: Into<String>>(mut self, key: S, header: OrReference<Header>) -> Self {
        if self.headers.is_none() {
            self.headers = Some(HashMap::new());
        }

        self.headers.as_mut().unwrap().insert(key.into(), header);

        self
    }

    pub fn with_headers<S: Into<String>>(
        mut self,
        headers: impl IntoIterator<Item = (S, OrReference<Header>)>,
    ) -> Self {
        if self.headers.is_none() {
            self.headers = Some(HashMap::new());
        }

        self.headers.as_mut().unwrap().extend(headers.into_iter().map(|(k, v)| (k.into(), v)));

        self
    }

    pub fn with_content<S: Into<String>>(mut self, key: S, content: MediaType) -> Self {
        if self.content.is_none() {
            self.content = Some(HashMap::new());
        }

        self.content.as_mut().unwrap().insert(key.into(), content);

        self
    }

    pub fn with_contents<S: Into<String>>(
        mut self,
        contents: impl IntoIterator<Item = (S, MediaType)>,
    ) -> Self {
        if self.content.is_none() {
            self.content = Some(HashMap::new());
        }

        self.content.as_mut().unwrap().extend(contents.into_iter().map(|(k, v)| (k.into(), v)));

        self
    }

    pub fn with_link<S: Into<String>>(mut self, key: S, link: OrReference<Link>) -> Self {
        if self.links.is_none() {
            self.links = Some(HashMap::new());
        }

        self.links.as_mut().unwrap().insert(key.into(), link);

        self
    }

    pub fn with_links<S: Into<String>>(
        mut self,
        links: impl IntoIterator<Item = (S, OrReference<Link>)>,
    ) -> Self {
        if self.links.is_none() {
            self.links = Some(HashMap::new());
        }

        self.links.as_mut().unwrap().extend(links.into_iter().map(|(k, v)| (k.into(), v)));

        self
    }
}

#[derive(Debug, PartialEq, Clone, Serialize, Deserialize)]
#[repr(transparent)]
#[serde(rename_all = "camelCase")]
pub struct Callback(HashMap<String, OrReference<PathItem>>);

#[derive(Debug, PartialEq, Clone, Serialize, Deserialize, Default)]
#[serde(rename_all = "camelCase")]
pub struct Example {
    pub summary: Option<String>,
    pub description: Option<String>,
    pub value: Option<serde_json::Value>,
    pub external_value: Option<String>,

    #[serde(flatten)]
    pub extensions: HashMap<String, serde_json::Value>,
}

impl Example {
    pub fn with_summary<S: Into<String>>(mut self, summary: S) -> Self {
        self.summary = Some(summary.into());
        self
    }

    pub fn with_description<S: Into<String>>(mut self, description: S) -> Self {
        self.description = Some(description.into());
        self
    }

    pub fn with_value(mut self, value: serde_json::Value) -> Self {
        self.value = Some(value);
        self
    }

    pub fn with_external_value<S: Into<String>>(mut self, external_value: S) -> Self {
        self.external_value = Some(external_value.into());
        self
    }
}

#[derive(Debug, PartialEq, Clone, Serialize, Deserialize, Default)]
#[serde(rename_all = "camelCase")]
pub struct Link {
    pub operation_ref: Option<String>,
    pub operation_id: Option<String>,
    pub parameters: Option<HashMap<String, OrExpression<serde_json::Value>>>,
    pub request_body: Option<OrExpression<serde_json::Value>>,
    pub description: Option<String>,
    pub server: Option<Server>,

    #[serde(flatten)]
    pub extensions: HashMap<String, serde_json::Value>,
}

#[derive(Debug, PartialEq, Clone, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
#[serde(untagged)]
pub enum HeaderRule {
    SchemaStyle {
        style: Option<String>,
        explode: Option<bool>,
        allow_reserved: Option<bool>,
        schema: Schema,
        example: Option<serde_json::Value>,
        examples: Option<HashMap<String, OrReference<Example>>>,
    },
    Content {
        content: HashMap<String, MediaType>,
    },
}

#[derive(Debug, PartialEq, Clone, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Header {
    pub description: Option<String>,
    pub required: Option<bool>,
    pub deprecated: Option<bool>,
    pub allow_empty_value: Option<bool>,

    #[serde(flatten)]
    pub parameter_rule: HeaderRule,

    #[serde(flatten)]
    pub extensions: HashMap<String, serde_json::Value>,
}

#[derive(Debug, PartialEq, Clone, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Tag {
    pub name: String,
    pub description: Option<String>,
    pub external_docs: Option<ExternalDocumentation>,

    #[serde(flatten)]
    pub extensions: HashMap<String, serde_json::Value>,
}

#[derive(Debug, PartialEq, Clone, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Reference {
    #[serde(rename = "$ref")]
    pub ref_: String,
    pub summary: Option<String>,
    pub description: Option<String>,
}

#[derive(Debug, PartialEq, Clone, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Schema {
    pub description: Option<String>,
    pub format: Option<String>,
    pub discriminator: Option<Discriminator>,
    pub xml: Option<XML>,
    pub external_docs: Option<ExternalDocumentation>,
    pub example: Option<serde_json::Value>,

    #[serde(flatten)]
    pub inner_schema: serde_json::Value,

    #[serde(flatten)]
    pub extensions: HashMap<String, serde_json::Value>,
}

#[derive(Debug, PartialEq, Clone, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Discriminator {
    pub property_name: String,
    pub mapping: Option<HashMap<String, String>>,

    #[serde(flatten)]
    pub extensions: HashMap<String, serde_json::Value>,
}

#[derive(Debug, PartialEq, Clone, Serialize, Deserialize, Default)]
#[serde(rename_all = "camelCase")]
pub struct XML {
    pub name: Option<String>,
    pub namespace: Option<String>,
    pub prefix: Option<String>,
    pub attribute: Option<String>,
    pub wrapped: Option<String>,

    #[serde(flatten)]
    pub extensions: HashMap<String, serde_json::Value>,
}

// TODO: This structure does not support specification extensions
#[derive(Debug, PartialEq, Clone, Serialize, Deserialize)]
#[serde(tag = "type")]
#[serde(rename_all = "camelCase")]
pub enum SecurityScheme {
    ApiKey {
        description: Option<String>,
        name: String,

        #[serde(rename = "in")]
        in_: String,
    },
    Http {
        description: Option<String>,
        scheme: String,
        bearerFormat: Option<String>,
    },

    #[serde(rename = "mutualTLS")]
    MutualTLS { description: Option<String> },

    #[serde(rename = "oauth2")]
    OAuth2 {
        description: Option<String>,
        flows: OAuthFlows,
    },

    OpenIdConnect {
        description: Option<String>,

        #[serde(rename = "openIdConnectUrl")]
        open_id_connect_url: Option<String>,
    },
}

#[derive(Debug, PartialEq, Clone, Serialize, Deserialize, Default)]
#[serde(rename_all = "camelCase")]
pub struct OAuthFlows {
    pub implicit: Option<OAuthFlow>,
    pub password: Option<OAuthFlow>,
    pub client_credentials: Option<OAuthFlow>,
    pub authorization_code: Option<OAuthFlow>,

    #[serde(flatten)]
    pub extensions: HashMap<String, serde_json::Value>,
}

#[derive(Debug, PartialEq, Clone, Serialize, Deserialize, Default)]
#[serde(rename_all = "camelCase")]
pub struct OAuthFlow {
    pub authorization_url: Option<String>,
    pub token_url: Option<String>,
    pub refresh_url: Option<String>,
    pub scopes: Option<HashMap<String, String>>,

    #[serde(flatten)]
    pub extensions: HashMap<String, serde_json::Value>,
}

#[derive(Debug, PartialEq, Clone, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct SecurityRequirements(HashMap<String, Vec<String>>);

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn info() {
        let json = r#"{
  "title": "Sample Pet Store App",
  "summary": "A pet store manager.",
  "description": "This is a sample server for a pet store.",
  "termsOfService": "https://example.com/terms/",
  "contact": {
    "name": "API Support",
    "url": "https://www.example.com/support",
    "email": "support@example.com"
  },
  "license": {
    "name": "Apache 2.0",
    "url": "https://www.apache.org/licenses/LICENSE-2.0.html"
  },
  "version": "1.0.1"
}"#;
        let parsed: Info = serde_json::from_str(json).unwrap();
    }
}
