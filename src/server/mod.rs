use http::{header, HeaderValue, StatusCode, Version};
use log::{error, info};
use std::{
    sync::{atomic::AtomicUsize, Arc},
    time::{Duration, Instant, SystemTime},
};
use tls::MaybeTlsStream;
use tokio::{io::BufStream, net::TcpListener};
use tokio_util::sync::CancellationToken;

#[cfg(feature = "tls")]
use tokio_rustls::{rustls, TlsAcceptor};

#[cfg(feature = "http3")]
use h3::server::Connection;

#[cfg(feature = "http3")]
use quinn::crypto::rustls::QuicServerConfig;

use crate::{stream::Stream, Config, Router};

pub use http;

pub(crate) mod http_impl;
pub(crate) mod tls;

use http_impl::{HttpImpl, Request, HTTP1};

#[cfg(feature = "http2")]
use http_impl::HTTP2;

#[cfg(feature = "http2")]
const ALPN_PROTOCOL_H2: &[u8] = b"h2";

#[cfg(feature = "http2")]
const ALPN_SUPPORTED_PROTOCOLS: &[&[u8]] = &[ALPN_PROTOCOL_H2, b"http/1.1", b"http/1.0"];

#[cfg(all(feature = "tls", not(feature = "http2")))]
const ALPN_SUPPORTED_PROTOCOLS: &[&[u8]] = &[b"http/1.1", b"http/1.0"];

#[cfg(feature = "http3")]
use http_impl::HTTP3;

#[cfg(feature = "http3")]
const ALPN_PROTOCOL_H3: &[u8] = b"h3";

const MAX_CONN_ERROR_COUNT: usize = 3;

const SERVER_NAME: &str = "papero";

#[cfg(debug_assertions)]
const DEBUG_ERROR_TEMPLATE: &str = r#"<!DOCTYPE HTML>
<html>
  <head>
  </head>
  <body>
    <h1>Server error</h1>
    <pre style="font-family: monospace, sans-serif;"><span style="color: red; font-weight: 600;"><code style="background-color: #F5F5F5; width: max-content; display: inline-block; padding: 14px; line-height: 1.5;">{error}</code></pre>
  </body>
</html>"#;

/// Maximum number of headers parsed in a request/response.
///
/// When a request/response is parsed, only 32 headers can be parsed. If the parsing fails because
/// there are more headers, it will be retried but with the limit defined by [`MAX_HEADERS`].
///
/// You can increase this value if needed using `papero::server::MAX_HEADERS.store(128, Ordering::Relaxed);`
/// but keep in mind that allowing too much headers will result in the server being more vulnerable
/// to DDoS attacks.
///
/// The default value is `64`.
pub static MAX_HEADERS: AtomicUsize = AtomicUsize::new(64);

/// Maximum size of the buffer used to read the stream.
///
/// You can increase this value if needed using
/// `papero::server::MAX_BUFFER_SIZE.store(1024 * 32, Ordering::Relaxed);` but keep in mind that
/// allowing too buffer size will result in the server being more vulnerable to DDoS attacks.
///
/// The default value is 16 KiB.
pub static MAX_BUFFER_SIZE: AtomicUsize = AtomicUsize::new(1024 * 16);

/// Maximum duration of a Keep-Alive HTTP/1.1 connection.
///
/// The default value is 30 seconds.
pub static MAX_KEEPALIVE_TIMEOUT: Duration = Duration::from_secs(30);

#[must_use]
#[derive(Debug, PartialEq)]
pub(crate) enum ConnectionState {
    KeepAlive,
    Close,
}

pub struct Server {
    config: Arc<Config>,
    router: Arc<Router>,
}

impl Server {
    pub fn new(config: Config, router: Router) -> Self {
        Self {
            config: Arc::new(config),
            router: Arc::new(router),
        }
    }

    pub(crate) async fn handle_request(
        router: &Router,
        stream: &mut Stream,
        mut req: Request,
    ) -> crate::Result<()> {
        let (handler, params) = router.at(&req);

        let extensions = req.extensions_mut();
        extensions.insert(params);

        // TODO: Avoid unwrapping
        stream.get_mut_extra().headers.insert(
            header::DATE,
            HeaderValue::from_str(
                httpdate::HttpDate::from(SystemTime::now())
                    .to_string()
                    .as_str(),
            )
            .unwrap(),
        );

        stream
            .get_mut_extra()
            .headers
            .insert(header::SERVER, HeaderValue::from_static(SERVER_NAME));

        let uri = req.uri().to_string();

        if let Err(e) = handler.call(req, stream).await {
            let error_string = e.to_string();
            error!(
                "Error in route handler for uri \"{}\": \"{}\"",
                uri, error_string
            );

            #[cfg(debug_assertions)]
            stream
                .send(
                    http::Response::builder()
                        .status(StatusCode::INTERNAL_SERVER_ERROR)
                        .header(header::CONTENT_TYPE, "text/html")
                        .body(DEBUG_ERROR_TEMPLATE.replace("{error}", &error_string))
                        .expect("failed to build response"),
                )
                .await?;

            // TODO: Add a way to deactivate this
            #[cfg(not(debug_assertions))]
            stream
                .send(
                    http::Response::builder()
                        .status(StatusCode::INTERNAL_SERVER_ERROR)
                        .header(header::CONTENT_TYPE, "text/plain")
                        .body(e.to_string())
                        .expect("failed to build response"),
                )
                .await?;
        }

        Ok(())
    }

    async fn handle_requests(
        mut stream: Stream,
        router: Arc<Router>,
        cancellation_token: CancellationToken,
    ) -> crate::Result<()> {
        #[cfg(not(feature = "http2"))]
        let _ = cancellation_token;

        let mut err_count = 0;

        loop {
            let start_time = Instant::now();

            if stream.cancel_timeout_token.is_cancelled() {
                // The stream was converted, it could not be used anymore
                return Ok(());
            }

            match stream.as_mut_http().parse_request().await {
                Ok(Some(req)) => {
                    // TODO: Maybe display the method while taking into account the _method query
                    // parameter
                    info!("{} {} {:?}", req.method(), req.uri(), req.version());

                    // Implement the Connection header (https://www.rfc-editor.org/rfc/rfc2616#section-14.10)
                    let connection_state = match req.headers().get(header::CONNECTION) {
                        Some(v) if v == "close" => ConnectionState::Close,
                        Some(v) if v.to_str().unwrap().to_lowercase() == "keep-alive" => {
                            ConnectionState::KeepAlive
                        }

                        // Keep-Alive is the default for HTTP/1.1 but not HTTP/1.0 nor HTTP/0.9
                        _ if req.version() == Version::HTTP_11 => ConnectionState::KeepAlive,
                        _ => ConnectionState::Close,
                    };

                    if stream.http_version() == Version::HTTP_11 {
                        stream.get_mut_extra().headers.insert(
                            header::CONNECTION,
                            match connection_state {
                                ConnectionState::KeepAlive => {
                                    HeaderValue::from_static("keep-alive")
                                }
                                ConnectionState::Close => HeaderValue::from_static("close"),
                            },
                        );
                    }

                    Server::handle_request(&router, &mut stream, req).await?;

                    let elapsed = start_time.elapsed();
                    info!("Responded in {}", humantime::format_duration(elapsed));

                    if connection_state == ConnectionState::Close {
                        break;
                    }

                    stream.get_mut_extra().headers.clear();
                }
                Ok(None) => break,

                #[cfg(feature = "http2")]
                Err(crate::error::HttpImplError::SwitchToHttp2(req_buf)) => {
                    let HttpImpl::HTTP1(http_impl) = stream.into_http() else {
                        unreachable!("HTTP version was already checked");
                    };

                    stream = Stream::from_http(
                        HttpImpl::HTTP2(HTTP2::from_http1_stream(http_impl, req_buf)),
                        cancellation_token.clone(),
                    );

                    let req = stream.as_mut_http().parse_request().await.unwrap().unwrap();
                    Server::handle_request(&router, &mut stream, req).await?;
                }

                Err(e) => {
                    if err_count + 1 > MAX_CONN_ERROR_COUNT {
                        break;
                    }

                    error!("Error when serving request: {e}");
                    err_count += 1;
                }
            }
        }

        Ok(())
    }

    async fn handle_connection(router: Arc<Router>, stream: MaybeTlsStream) -> crate::Result<()> {
        // A cancellation token used to prevent timeout when a stream is converted.
        // Useful when the stream is upgraded to a longer-lived connection, e.g in websockets
        let cancel_timeout_token = CancellationToken::new();

        #[cfg(feature = "http2")]
        let stream = {
            let is_http2_conn = if let MaybeTlsStream::WithTls(tls_stream) = &stream {
                if let Some(alpn_protocol) = tls_stream.get_ref().get_ref().1.alpn_protocol() {
                    alpn_protocol == ALPN_PROTOCOL_H2
                } else {
                    // Invalid ALPN protocol, defaulting to HTTP/1
                    false
                }
            } else {
                false
            };

            Stream::from_http(
                if is_http2_conn {
                    HttpImpl::HTTP2(HTTP2::from_stream(stream))
                } else {
                    HttpImpl::HTTP1(HTTP1::from_stream(stream))
                },
                cancel_timeout_token.clone(),
            )
        };

        #[cfg(not(feature = "http2"))]
        let stream = Stream::from_http(
            HttpImpl::HTTP1(HTTP1::from_stream(stream)),
            cancel_timeout_token.clone(),
        );

        let timeout_token = CancellationToken::new();

        {
            let timeout_token = timeout_token.clone();
            let cancel_timeout_token = cancel_timeout_token.clone();
            tokio::spawn(async move {
                tokio::select! {
                    _ = cancel_timeout_token.cancelled() => {
                    },
                    _ = tokio::time::sleep(MAX_KEEPALIVE_TIMEOUT) => {
                        timeout_token.cancel();
                    }
                }
            });
        }

        tokio::select! {
            _ = timeout_token.cancelled() => {
                Ok(())
            }
            r = Self::handle_requests(stream, router, cancel_timeout_token) => {
                r
            },
        }
    }

    pub async fn run(&self) -> crate::Result<()> {
        #[cfg(feature = "tls")]
        let https_server_future = if let Some(tls_config) = &self.config.tls {
            let certs = tls::load_certs(&tls_config.cert_path)?;
            let key = tls::load_key(&tls_config.key_path)?;
            let mut tls_config = rustls::ServerConfig::builder()
                .with_no_client_auth()
                .with_single_cert(certs, key)
                .map_err(|err| std::io::Error::new(std::io::ErrorKind::InvalidInput, err))?;
            tls_config.alpn_protocols = ALPN_SUPPORTED_PROTOCOLS
                .iter()
                .map(|v| v.to_vec())
                .collect();

            let https_listener =
                TcpListener::bind(&self.config.https_listen_addr.as_slice()).await?;
            let tls_config_cloned = tls_config.clone();
            let acceptor = TlsAcceptor::from(Arc::new(tls_config));

            Some((tls_config_cloned, async move {
                loop {
                    let acceptor = acceptor.clone();
                    let (stream, _) = https_listener.accept().await?;

                    let router = self.router.clone();

                    tokio::spawn(async move {
                        let closure = || async move {
                            let stream = MaybeTlsStream::WithTls(BufStream::new(
                                acceptor.accept(stream).await?,
                            ));
                            Self::handle_connection(router, stream).await
                        };

                        if let Err(e) = closure().await {
                            error!("Error in worker task: {e:?}");
                        }
                    });
                }

                #[allow(unreachable_code)]
                Ok::<(), crate::Error>(())
            }))
        } else {
            None
        };

        #[cfg(not(feature = "tls"))]
        let https_server_future: Option<(
            (),
            futures::future::BoxFuture<crate::Result<()>>,
        )> = None;

        let http_listener = TcpListener::bind(self.config.http_listen_addr.as_slice()).await?;

        let http_server_future: futures::future::BoxFuture<crate::Result<()>> =
            if self.config.tls.is_some() && self.config.redirect_to_https.is_some() {
                Box::pin(async move {
                    loop {
                        let (stream, _) = http_listener.accept().await?;

                        let config = self.config.clone();

                        tokio::spawn(async move {
                            let closure = || async move {
                                // It is safely possible to send a plain HTTP/1.1 response because
                                // the stream is not using TLS and if the request has an `Upgrade`
                                // header to use HTTP/2, it can be safely ignored
                                let stream = MaybeTlsStream::WithoutTls(BufStream::new(stream));
                                let mut http_impl = HttpImpl::HTTP1(HTTP1::from_stream(stream));
                                let _req = http_impl.parse_request().await;

                                let mut stream =
                                    Stream::from_http(http_impl, CancellationToken::new());
                                crate::utils::redirect_to(
                                    &mut stream,
                                    config.redirect_to_https.as_ref().unwrap(),
                                )
                                .await?;

                                Ok::<(), crate::Error>(())
                            };

                            if let Err(e) = closure().await {
                                error!("Error in worker task: {e:?}");
                            }
                        });
                    }

                    #[allow(unreachable_code)]
                    Ok(())
                })
            } else {
                Box::pin(async move {
                    loop {
                        let (stream, _) = http_listener.accept().await?;

                        let router = self.router.clone();

                        tokio::spawn(async move {
                            let closure = || async move {
                                let stream = MaybeTlsStream::WithoutTls(BufStream::new(stream));
                                Self::handle_connection(router, stream).await
                            };

                            if let Err(e) = closure().await {
                                error!("Error in worker task: {e:?}");
                            }
                        });
                    }

                    #[allow(unreachable_code)]
                    Ok(())
                })
            };

        #[cfg(feature = "http3")]
        let http3_server_future = if let Some(http3_listen_addr) = &self.config.http3_listen_addr {
            if let Some(mut tls_config) = https_server_future.as_ref().map(|f| f.0.clone()) {
                tls_config.alpn_protocols = vec![ALPN_PROTOCOL_H3.to_vec()];
                tls_config.max_early_data_size = u32::MAX;

                // TODO: let the user choose the cipher suite
                let server_config = quinn::ServerConfig::with_crypto(Arc::new(
                    QuicServerConfig::try_from(tls_config)
                        .map_err(crate::error::ServerError::BadQuicServerConfig)?,
                ));

                // TODO: Avoid unwrapping
                let endpoint =
                    quinn::Endpoint::server(server_config, *http3_listen_addr.first().unwrap())?;

                Some(async move {
                    loop {
                        let stream = endpoint
                            .accept()
                            .await
                            .ok_or_else(|| crate::error::ServerError::Accept)?;

                        let router = self.router.clone();

                        tokio::spawn(async move {
                            let closure = || async move {
                                let conn =
                                    stream.await.map_err(crate::error::ServerError::Quinn)?;
                                let stream: Connection<h3_quinn::Connection, bytes::Bytes> =
                                    h3::server::Connection::new(h3_quinn::Connection::new(conn))
                                        .await
                                        .unwrap();

                                let mut stream = Stream::from_http(
                                    HttpImpl::HTTP3(HTTP3::from_stream(stream)),
                                    CancellationToken::new(),
                                );

                                let mut err_count = 0;

                                loop {
                                    let start_time = Instant::now();

                                    match stream.as_mut_http().parse_request().await {
                                        Ok(Some(req)) => {
                                            info!(
                                                "{} {} {:?}",
                                                req.method(),
                                                req.uri(),
                                                req.version()
                                            );

                                            // TODO: Avoid unwrapping
                                            stream.get_mut_extra().headers.insert(
                                                header::DATE,
                                                HeaderValue::from_str(
                                                    httpdate::HttpDate::from(SystemTime::now())
                                                        .to_string()
                                                        .as_str(),
                                                )
                                                .unwrap(),
                                            );

                                            stream.get_mut_extra().headers.insert(
                                                header::SERVER,
                                                HeaderValue::from_static(SERVER_NAME),
                                            );

                                            Self::handle_request(&router, &mut stream, req).await?;

                                            let elapsed = start_time.elapsed();
                                            info!(
                                                "Responded in {}",
                                                humantime::format_duration(elapsed)
                                            );

                                            stream.get_mut_extra().headers.clear();
                                        }
                                        Ok(None) => break,
                                        Err(e) => {
                                            if err_count + 1 > MAX_CONN_ERROR_COUNT {
                                                break;
                                            }

                                            error!("Error when serving request: {e}");
                                            err_count += 1;
                                        }
                                    }
                                }

                                Ok::<(), crate::Error>(())
                            };

                            if let Err(e) = closure().await {
                                error!("Error in worker task: {e:?}");
                            }
                        });
                    }

                    #[allow(unreachable_code)]
                    Ok::<(), crate::Error>(())
                })
            } else {
                None
            }
        } else {
            None
        };

        #[cfg(not(feature = "http3"))]
        let http3_server_future: Option<futures::future::BoxFuture<crate::Result<()>>> = None;

        if let Some(http3_server_future) = http3_server_future {
            if let Some(https_server_future) = https_server_future {
                let (http_res, https_res, http3_res) = futures::join!(
                    http_server_future,
                    https_server_future.1,
                    http3_server_future
                );
                http_res?;
                https_res?;
                http3_res?;
                Ok(())
            } else {
                let (http_res, http3_res) = futures::join!(http_server_future, http3_server_future);
                http_res?;
                http3_res?;
                Ok(())
            }
        } else if let Some(https_server_future) = https_server_future {
            let (http_res, https_res) = futures::join!(http_server_future, https_server_future.1);
            http_res?;
            https_res?;
            Ok(())
        } else {
            http_server_future.await
        }
    }
}
