use std::{
    io,
    pin::Pin,
    task::{Context, Poll},
};

use tokio::io::{AsyncRead, AsyncWrite, BufStream, ReadBuf};

#[cfg(feature = "tls")]
use tokio_rustls::rustls::pki_types::{CertificateDer, PrivateKeyDer};

#[derive(Debug)]
pub enum MaybeTlsStream {
    #[cfg(feature = "tls")]
    WithTls(BufStream<tokio_rustls::server::TlsStream<tokio::net::TcpStream>>),

    WithoutTls(BufStream<tokio::net::TcpStream>),
}

impl AsyncRead for MaybeTlsStream {
    #[inline]
    fn poll_read(
        mut self: Pin<&mut Self>,
        cx: &mut Context<'_>,
        buf: &mut ReadBuf<'_>,
    ) -> Poll<io::Result<()>> {
        match &mut *self {
            #[cfg(feature = "tls")]
            MaybeTlsStream::WithTls(tls_stream) => {
                AsyncRead::poll_read(Pin::new(tls_stream), cx, buf)
            }
            MaybeTlsStream::WithoutTls(stream) => AsyncRead::poll_read(Pin::new(stream), cx, buf),
        }
    }
}

impl AsyncWrite for MaybeTlsStream {
    #[inline]
    fn poll_write(
        mut self: Pin<&mut Self>,
        cx: &mut Context<'_>,
        buf: &[u8],
    ) -> Poll<Result<usize, io::Error>> {
        match &mut *self {
            #[cfg(feature = "tls")]
            MaybeTlsStream::WithTls(tls_stream) => {
                AsyncWrite::poll_write(Pin::new(tls_stream), cx, buf)
            }
            MaybeTlsStream::WithoutTls(stream) => AsyncWrite::poll_write(Pin::new(stream), cx, buf),
        }
    }

    #[inline]
    fn poll_flush(mut self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Result<(), io::Error>> {
        match &mut *self {
            #[cfg(feature = "tls")]
            MaybeTlsStream::WithTls(tls_stream) => AsyncWrite::poll_flush(Pin::new(tls_stream), cx),
            MaybeTlsStream::WithoutTls(stream) => AsyncWrite::poll_flush(Pin::new(stream), cx),
        }
    }

    #[inline]
    fn poll_shutdown(
        mut self: Pin<&mut Self>,
        cx: &mut Context<'_>,
    ) -> Poll<Result<(), io::Error>> {
        match &mut *self {
            #[cfg(feature = "tls")]
            MaybeTlsStream::WithTls(tls_stream) => {
                AsyncWrite::poll_shutdown(Pin::new(tls_stream), cx)
            }
            MaybeTlsStream::WithoutTls(stream) => AsyncWrite::poll_shutdown(Pin::new(stream), cx),
        }
    }
}

#[cfg(feature = "tls")]
pub(crate) fn load_certs(path: &std::path::Path) -> crate::Result<Vec<CertificateDer<'static>>> {
    rustls_pemfile::certs(&mut std::io::BufReader::new(std::fs::File::open(path)?))
        .map(|v| v.map_err(|e| e.into()))
        .collect()
}

#[cfg(feature = "tls")]
pub(crate) fn load_key(path: &std::path::Path) -> crate::Result<PrivateKeyDer<'static>> {
    rustls_pemfile::private_key(&mut std::io::BufReader::new(std::fs::File::open(path)?))
        .unwrap()
        .ok_or(crate::error::Error::Server(
            crate::error::ServerError::NoPrivateKey,
        ))
}
