use bytes::{Buf, Bytes};
use h3::server::{Connection, RequestStream};

use super::{Request, Response};

pub struct HTTP3 {
    stream: Option<Connection<h3_quinn::Connection, Bytes>>,
    send_res: Option<RequestStream<h3_quinn::BidiStream<Bytes>, Bytes>>,
}

impl HTTP3 {
    pub fn from_stream(stream: Connection<h3_quinn::Connection, Bytes>) -> Self {
        Self {
            stream: Some(stream),
            send_res: None,
        }
    }

    pub async fn parse_request(&mut self) -> super::Result<Option<Request>> {
        let (req, mut req_stream) = match self.stream.as_mut().unwrap().accept().await {
            Ok(Some(r)) => r,
            Ok(None) => return Ok(None),
            Err(e)
                if matches!(
                    e.kind(),
                    h3::error::Kind::Timeout | h3::error::Kind::Closed | h3::error::Kind::Closing
                ) =>
            {
                return Ok(None)
            }
            Err(e) => return Err(crate::error::HttpImplError::Http3(e)),
        };

        let (parts, _) = req.into_parts();
        let mut buf = vec![];

        while let Some(chunk) = req_stream
            .recv_data()
            .await
            .map_err(crate::error::HttpImplError::Http3)?
        {
            buf.extend(chunk.chunk().to_vec());
        }

        self.send_res = Some(req_stream);

        let req = Request::from_parts(parts, buf);
        Ok(Some(req))
    }

    pub async fn send_request(&mut self, _req: Request) -> super::Result<()> {
        // TODO: Implement an HTTP/3 client
        todo!()
    }

    pub async fn parse_response(&mut self) -> super::Result<Option<Response>> {
        // TODO: Implement an HTTP/3 client
        todo!()
    }

    pub async fn send_response<T: AsRef<[u8]> + Send>(
        &mut self,
        res: http::Response<T>,
    ) -> super::Result<()> {
        {
            let send_res = self
                .send_res
                .as_mut()
                .ok_or(crate::error::HttpImplError::RequestRequiredForResponse)?;
            let (parts, body) = res.into_parts();

            send_res
                .send_response(http::Response::from_parts(parts, ()))
                .await
                .map_err(crate::error::HttpImplError::Http3)?;

            // TODO: Cut in chunks
            send_res
                .send_data(body.as_ref().to_vec().into())
                .await
                .map_err(crate::error::HttpImplError::Http3)?;

            send_res
                .finish()
                .await
                .map_err(crate::error::HttpImplError::Http3)?;
        }

        Ok(())
    }
}
