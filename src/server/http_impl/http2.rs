use std::{
    io,
    sync::{Arc, Mutex},
    task::Poll,
};

use bytes::Bytes;
use h2::{
    client::{ResponseFuture, SendRequest},
    server::{Connection, SendResponse},
    Reason,
};
use tokio::io::{AsyncRead, AsyncWrite};

use crate::server::tls::MaybeTlsStream;

use super::{Request, Response};

#[derive(Debug)]
struct RewindStream {
    first_data: Arc<Mutex<Option<Vec<u8>>>>,
    inner: MaybeTlsStream,
}

impl AsyncRead for RewindStream {
    fn poll_read(
        mut self: std::pin::Pin<&mut Self>,
        cx: &mut std::task::Context<'_>,
        buf: &mut tokio::io::ReadBuf<'_>,
    ) -> std::task::Poll<io::Result<()>> {
        if self.first_data.lock().unwrap().is_none() {
            return AsyncRead::poll_read(std::pin::Pin::new(&mut self.inner), cx, buf);
        }

        let mut first_data = self.first_data.lock().unwrap().take().unwrap();

        let rest = if buf.remaining() < first_data.len() {
            first_data.split_off(buf.remaining())
        } else {
            vec![]
        };
        buf.put_slice(&first_data);

        if !rest.is_empty() {
            *self.first_data.lock().unwrap() = Some(rest);
        }

        Poll::Ready(Ok(()))
    }
}

impl AsyncWrite for RewindStream {
    fn poll_write(
        mut self: std::pin::Pin<&mut Self>,
        cx: &mut std::task::Context<'_>,
        buf: &[u8],
    ) -> std::task::Poll<Result<usize, io::Error>> {
        AsyncWrite::poll_write(std::pin::Pin::new(&mut self.inner), cx, buf)
    }

    fn poll_flush(
        mut self: std::pin::Pin<&mut Self>,
        cx: &mut std::task::Context<'_>,
    ) -> std::task::Poll<Result<(), io::Error>> {
        AsyncWrite::poll_flush(std::pin::Pin::new(&mut self.inner), cx)
    }

    fn poll_shutdown(
        mut self: std::pin::Pin<&mut Self>,
        cx: &mut std::task::Context<'_>,
    ) -> std::task::Poll<Result<(), io::Error>> {
        AsyncWrite::poll_shutdown(std::pin::Pin::new(&mut self.inner), cx)
    }
}

#[derive(Debug)]
pub struct HTTP2 {
    stream: Option<RewindStream>,
    conn: Option<Connection<RewindStream, Bytes>>,
    send_req: Option<SendRequest<Bytes>>,
    send_res: Option<SendResponse<Bytes>>,
    recv_res: Option<ResponseFuture>,
}

impl HTTP2 {
    pub fn from_stream(stream: MaybeTlsStream) -> Self {
        Self {
            stream: Some(RewindStream {
                first_data: Arc::new(Mutex::new(None)),
                inner: stream,
            }),
            conn: None,
            send_req: None,
            send_res: None,
            recv_res: None,
        }
    }

    pub fn from_http1_stream(stream: super::HTTP1, request_buffer: Vec<u8>) -> Self {
        Self {
            stream: Some(RewindStream {
                first_data: Arc::new(Mutex::new(Some(request_buffer))),
                inner: stream.into_stream(),
            }),
            conn: None,
            send_req: None,
            send_res: None,
            recv_res: None,
        }
    }

    pub async fn parse_request(&mut self) -> super::Result<Option<Request>> {
        if self.conn.is_none() {
            let stream = self
                .stream
                .take()
                .ok_or(crate::error::HttpImplError::MixedClientServer)?;

            self.conn = Some(
                h2::server::handshake(stream)
                    .await
                    .map_err(crate::error::HttpImplError::Http2)?,
            );
        }

        let Some((req, send_res)) = self
            .conn
            .as_mut()
            .unwrap()
            .accept()
            .await
            .transpose()
            .map_err(crate::error::HttpImplError::Http2)?
        else {
            return Ok(None);
        };
        self.send_res = Some(send_res);

        let (parts, mut body) = req.into_parts();
        let mut buf = vec![];

        while !body.is_end_stream() {
            buf.extend(body.data().await.unwrap().unwrap());
        }

        let req = Request::from_parts(parts, buf);
        Ok(Some(req))
    }

    pub async fn send_request(&mut self, req: Request) -> super::Result<()> {
        if self.send_req.is_none() {
            let stream = self
                .stream
                .take()
                .ok_or(crate::error::HttpImplError::MixedClientServer)?;

            let (send_req, worker) = h2::client::handshake(stream)
                .await
                .map_err(crate::error::HttpImplError::Http2)?;

            tokio::spawn(async move {
                worker.await.unwrap();
            });

            let send_req = send_req
                .ready()
                .await
                .map_err(crate::error::HttpImplError::Http2)?;
            self.send_req = Some(send_req)
        }

        let (parts, body) = req.into_parts();
        let (recv_res, mut send_res) = self
            .send_req
            .as_mut()
            .unwrap()
            .send_request(http::Request::from_parts(parts, ()), false)
            .map_err(crate::error::HttpImplError::Http2)?;
        send_res
            .send_data(body.into(), true)
            .map_err(crate::error::HttpImplError::Http2)?;
        self.recv_res = Some(recv_res);

        Ok(())
    }

    pub async fn parse_response(&mut self) -> super::Result<Option<Response>> {
        let Some(recv_res) = self.recv_res.take() else {
            return Ok(None);
        };
        let res = recv_res.await.map_err(crate::error::HttpImplError::Http2)?;

        let (parts, mut body) = res.into_parts();
        let mut buf = vec![];

        while !body.is_end_stream() {
            buf.extend(body.data().await.unwrap().unwrap());
        }

        let res = Response::from_parts(parts, buf);
        Ok(Some(res))
    }

    pub async fn send_response<T: AsRef<[u8]> + Send>(
        &mut self,
        res: http::Response<T>,
    ) -> super::Result<()> {
        let send_res = self
            .send_res
            .as_mut()
            .ok_or(crate::error::HttpImplError::RequestRequiredForResponse)?;
        let (parts, body) = res.into_parts();

        let mut send_stream = send_res
            .send_response(http::Response::from_parts(parts, ()), false)
            .map_err(crate::error::HttpImplError::Http2)?;

        // TODO: Cut in chunks
        send_stream
            .send_data(body.as_ref().to_vec().into(), true)
            .map_err(crate::error::HttpImplError::Http2)?;

        // Useful to make progress on the stream
        // Unwrapping is guaranteed succeed because `send_res` is only `Some(...)` when
        // `self.conn` is `Some(...)`
        futures::future::lazy(|cx| {
            let _ = self.conn.as_mut().unwrap().poll_closed(cx);
        })
        .await;
        Ok(())
    }
}

impl Drop for HTTP2 {
    fn drop(&mut self) {
        if let Some(mut conn) = self.conn.take() {
            tokio::spawn(async move {
                futures::future::lazy(|cx| {
                    conn.abrupt_shutdown(Reason::STREAM_CLOSED);

                    loop {
                        if conn.poll_closed(cx).is_ready() {
                            break;
                        }
                    }
                })
                .await;
            });
        }
    }
}

#[cfg(test)]
mod tests {
    use http::{header, Method, StatusCode, Version};

    use crate::testing::{assert_requests_equality, assert_responses_equality, test_stream};

    use super::*;

    // TODO: Test connection reuse using a single HTTP2 structure to parse multiple requests
    // TODO: Test sending multiple responses on a single connection

    #[tokio::test]
    async fn parse_request() {
        let expected_request = http::Request::builder()
            .version(Version::HTTP_2)
            .uri("https://example.com/about")
            .method(Method::POST)
            .header(header::CONTENT_TYPE, "application/x-www-form-urlencoded")
            .body(())
            .unwrap();

        test_stream(|stream| async move {
            let (h2, conn) = h2::client::handshake(stream.server).await.unwrap();
            tokio::spawn(async move {
                conn.await.unwrap();
            });

            let mut h2 = h2.ready().await.unwrap();

            let (_, mut send_res) = h2.send_request(expected_request.clone(), false).unwrap();

            send_res
                .send_data(Bytes::from_static(b"user=john_doe"), true)
                .unwrap();

            let mut http_impl = HTTP2::from_stream(stream.client);
            let req = http_impl.parse_request().await.unwrap().unwrap();

            assert_requests_equality(expected_request.map(|_| b"user=john_doe".to_vec()), req);

            Ok(())
        })
        .await
        .unwrap();
    }

    #[tokio::test]
    async fn send_request() {
        let expected_request = http::Request::builder()
            .version(Version::HTTP_2)
            .uri("https://example.com/about")
            .method(Method::POST)
            .header(header::CONTENT_TYPE, "application/x-www-form-urlencoded")
            .body("user=john_doe".as_bytes().to_vec())
            .unwrap();

        test_stream(|stream| async move {
            let mut http_impl = HTTP2::from_stream(stream.client);
            http_impl.send_request(expected_request.clone()).await?;

            let mut http_impl = HTTP2::from_stream(stream.server);
            let req = http_impl.parse_request().await.unwrap().unwrap();

            assert_requests_equality(expected_request, req);
            Ok(())
        })
        .await
        .unwrap();
    }

    #[tokio::test]
    async fn parse_response() {
        let request = http::Request::builder()
            .version(Version::HTTP_2)
            .uri("https://example.com/about")
            .method(Method::POST)
            .header(header::CONTENT_TYPE, "application/x-www-form-urlencoded")
            .body("user=john_doe".as_bytes().to_vec())
            .unwrap();

        let expected_response = http::Response::builder()
            .version(Version::HTTP_2)
            .status(StatusCode::FOUND)
            .header(header::CONTENT_TYPE, "text/html")
            .body("<h1>Hello world!</h1>".as_bytes().to_vec())
            .unwrap();

        test_stream(|stream| async move {
            let mut http_impl_client = HTTP2::from_stream(stream.server);
            http_impl_client.send_request(request).await.unwrap();

            let mut http_impl_server = HTTP2::from_stream(stream.client);
            let _req = http_impl_server.parse_request().await.unwrap();
            http_impl_server
                .send_response(expected_response.clone())
                .await
                .unwrap();

            let res = http_impl_client.parse_response().await.unwrap().unwrap();
            assert_responses_equality(res, expected_response);

            drop(http_impl_server);

            Ok(())
        })
        .await
        .unwrap();
    }

    #[tokio::test]
    async fn send_response() {
        let expected_response = http::Response::builder()
            .version(Version::HTTP_2)
            .status(StatusCode::FOUND)
            .header(header::CONTENT_TYPE, "text/html")
            .body("<h1>Hello world!</h1>".as_bytes().to_vec())
            .unwrap();

        let cloned_expected_response = expected_response.clone();
        let res = test_stream(|stream| async move {
            let (h2, conn) = h2::client::handshake(stream.server).await.unwrap();
            tokio::spawn(async move {
                conn.await.unwrap();
            });

            let mut h2 = h2.ready().await.unwrap();

            let (res, mut send_stream) = h2
                .send_request(
                    http::Request::builder()
                        .uri("/about")
                        .method(Method::POST)
                        .header(header::CONTENT_TYPE, "application/x-www-form-urlencoded")
                        .body(())
                        .unwrap(),
                    false,
                )
                .unwrap();

            send_stream
                .send_data(Bytes::from_static(b"user=john_doe"), true)
                .unwrap();

            let mut http_impl = HTTP2::from_stream(stream.client);
            let _req = http_impl.parse_request().await.unwrap();

            http_impl.send_response(cloned_expected_response).await?;

            drop(http_impl);

            let res = res.await.unwrap();
            let (parts, mut body) = res.into_parts();
            let mut buf = vec![];

            while !body.is_end_stream() {
                buf.extend(body.data().await.unwrap().unwrap());
            }

            let res = Response::from_parts(parts, buf);
            Ok(res)
        })
        .await
        .unwrap();

        assert_responses_equality(expected_response, res);
    }
}
