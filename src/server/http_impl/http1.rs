use std::{
    fmt::Write,
    io::{self, ErrorKind},
    sync::atomic::Ordering,
};

use http::{Method, StatusCode, Version};
use tokio::io::{AsyncReadExt, AsyncWriteExt};

use crate::server::tls::MaybeTlsStream;

use super::{Request, Response};

const TMP_BUFFER_SIZE: usize = 1024;

pub struct HTTP1 {
    stream: MaybeTlsStream,
}

impl HTTP1 {
    pub fn from_stream(stream: MaybeTlsStream) -> Self {
        Self { stream }
    }

    pub fn as_stream(&self) -> &MaybeTlsStream {
        &self.stream
    }

    pub fn as_mut_stream(&mut self) -> &mut MaybeTlsStream {
        &mut self.stream
    }

    pub fn into_stream(self) -> MaybeTlsStream {
        self.stream
    }

    pub async fn parse_request(&mut self) -> super::Result<Option<Request>> {
        let global_max_buf_size = crate::server::MAX_BUFFER_SIZE.load(Ordering::Relaxed);

        let mut buf = vec![];
        let mut tmp_buffer = [0u8; TMP_BUFFER_SIZE];

        // Read request prelude and headers
        let (req, request_start_length) = loop {
            let bytes_read = self.stream.read(&mut tmp_buffer).await?;

            if buf.len() + bytes_read > global_max_buf_size {
                return Err(crate::error::HttpImplError::RequestParsingBufferTooSmall(
                    buf.len() + bytes_read,
                ));
            }

            buf.extend(&tmp_buffer[..bytes_read]);

            // headers_vec is later used through `req.headers`
            #[allow(dead_code, unused_assignments)]
            let mut headers_vec = None;
            let mut headers = [httparse::EMPTY_HEADER; 32];
            let mut req = httparse::Request::new(&mut headers);

            let status = match req.parse(&buf) {
                Ok(s) => s,
                Err(httparse::Error::TooManyHeaders) => {
                    headers_vec = Some(vec![
                        httparse::EMPTY_HEADER;
                        crate::server::MAX_HEADERS.load(Ordering::Relaxed)
                    ]);
                    req = httparse::Request::new(headers_vec.as_mut().unwrap());
                    req.parse(&buf)
                        .map_err(crate::error::HttpImplError::Http1)?
                }
                Err(httparse::Error::Version) => {
                    #[cfg(feature = "http2")]
                    return Err(crate::error::HttpImplError::SwitchToHttp2(buf));

                    #[cfg(not(feature = "http2"))]
                    return Err(crate::error::HttpImplError::Http1(httparse::Error::Version));
                }
                Err(e) => return Err(crate::error::HttpImplError::Http1(e)),
            };

            let httparse::Status::Complete(request_start_length) = status else {
                if bytes_read == 0 {
                    return Ok(None);
                } else {
                    continue;
                }
            };

            let Some(method) = req.method else {
                return Ok(None);
            };
            let Some(path) = req.path else {
                return Ok(None);
            };
            let Some(version) = req.version else {
                return Ok(None);
            };

            let mut req_builder = http::Request::builder()
                .method(
                    method
                        .parse::<Method>()
                        .map_err(|e| io::Error::new(ErrorKind::InvalidData, e))?,
                )
                .uri(path)
                .version(match version {
                    0 => Version::HTTP_10,
                    1 => Version::HTTP_11,
                    _ => unimplemented!("httparse only supports HTTP/1.0 and HTTP/1.1"),
                });

            for header in req.headers {
                req_builder = req_builder.header(header.name, header.value);
            }

            let req = req_builder
                .body(())
                .map_err(|_| crate::error::HttpImplError::MalformedRequestData)?;

            break (req, request_start_length);
        };

        // Read request body
        // https://greenbytes.de/tech/webdav/rfc2616.html#message.length
        let length = if req
            .headers()
            .get(http::header::TRANSFER_ENCODING)
            .map(|v| v != "identity")
            == Some(true)
        {
            // Use chunked transfer-coding
            None
        } else if let Some(length) = req.headers().get(http::header::CONTENT_LENGTH) {
            Some(
                length
                    .to_str()
                    .map_err(crate::error::HttpImplError::HeaderValueToString)?
                    .parse::<usize>()
                    .map_err(|_| crate::error::HttpImplError::MalformedRequestData)?,
            )
        } else {
            // No Content-Length, assume no request body
            Some(0)
        };

        if let Some(mut length) = length {
            if length > global_max_buf_size {
                length = global_max_buf_size;
            }

            if buf.len() - request_start_length < length {
                loop {
                    let bytes_read = self.stream.read(&mut tmp_buffer).await?;
                    buf.extend(&tmp_buffer[..bytes_read]);

                    if buf.len() - request_start_length + bytes_read >= length || bytes_read == 0 {
                        break;
                    }
                }
            }

            Ok(Some(req.map(|_| buf.split_off(request_start_length))))
        } else {
            // https://greenbytes.de/tech/webdav/rfc2616.html#transfer.codings
            todo!("add support for chunked transfer-coding");
        }
    }

    pub async fn send_request(&mut self, req: Request) -> super::Result<()> {
        let mut buf = String::new();

        // `unwrap`s can't panic because writing to a String can't fail
        write!(buf, "{} ", req.method()).unwrap();
        write!(buf, "{} ", req.uri()).unwrap();
        write!(buf, "{:?}\r\n", req.version()).unwrap();

        for (name, val) in req.headers() {
            write!(
                buf,
                "{}: {}\r\n",
                name,
                val.to_str()
                    .map_err(crate::error::HttpImplError::HeaderValueToString)?
            )
            .unwrap();
        }

        write!(buf, "\r\n").unwrap();
        self.as_mut_stream().write_all(&buf.into_bytes()).await?;
        self.as_mut_stream().write_all(req.body()).await?;

        Ok(())
    }

    pub async fn parse_response(&mut self) -> super::Result<Option<Response>> {
        let mut buf = vec![];
        let mut tmp_buffer = [0u8; TMP_BUFFER_SIZE];

        loop {
            let bytes_read = self.stream.read(&mut tmp_buffer).await?;

            if buf.len() + bytes_read > crate::server::MAX_BUFFER_SIZE.load(Ordering::Relaxed) {
                return Err(crate::error::HttpImplError::RequestParsingBufferTooSmall(
                    buf.len() + bytes_read,
                ));
            }

            buf.extend(&tmp_buffer[..bytes_read]);

            // headers_vec is later used through `res.headers`
            #[allow(dead_code, unused_assignments)]
            let mut headers_vec = None;
            let mut headers = [httparse::EMPTY_HEADER; 32];
            let mut res = httparse::Response::new(&mut headers);

            let status = match res.parse(&buf) {
                Ok(s) => s,
                Err(httparse::Error::TooManyHeaders) => {
                    headers_vec = Some(vec![
                        httparse::EMPTY_HEADER;
                        crate::server::MAX_HEADERS.load(Ordering::Relaxed)
                    ]);
                    res = httparse::Response::new(headers_vec.as_mut().unwrap());
                    res.parse(&buf)
                        .map_err(crate::error::HttpImplError::Http1)?
                }
                Err(e) => return Err(crate::error::HttpImplError::Http1(e)),
            };

            let httparse::Status::Complete(response_start_length) = status else {
                if bytes_read == 0 {
                    return Ok(None);
                } else {
                    continue;
                }
            };

            let Some(code) = res.code else {
                return Ok(None);
            };
            let Some(version) = res.version else {
                return Ok(None);
            };

            let mut res_builder = http::Response::builder()
                .status(
                    StatusCode::from_u16(code)
                        .map_err(|e| io::Error::new(ErrorKind::InvalidData, e))?,
                )
                .version(match version {
                    0 => Version::HTTP_10,
                    1 => Version::HTTP_11,
                    _ => unimplemented!("httparse only supports HTTP/1.0 and HTTP/1.1"),
                });

            for header in res.headers {
                res_builder = res_builder.header(header.name, header.value);
            }

            return res_builder
                .body(buf.split_off(response_start_length))
                .map(Some)
                .map_err(|_| crate::error::HttpImplError::MalformedResponseData);
        }
    }

    pub async fn send_response<T: AsRef<[u8]> + Send>(
        &mut self,
        res: http::Response<T>,
    ) -> super::Result<()> {
        let mut buf = String::new();

        // `unwrap`s can't panic because writing to a String can't fail
        write!(buf, "{:?} ", res.version()).unwrap();
        write!(buf, "{}\r\n", res.status()).unwrap();

        for (name, val) in res.headers() {
            write!(
                buf,
                "{}: {}\r\n",
                name,
                val.to_str()
                    .map_err(crate::error::HttpImplError::HeaderValueToString)?
            )
            .unwrap();
        }

        write!(buf, "\r\n").expect("writing to a String can't fail");
        self.as_mut_stream().write_all(&buf.into_bytes()).await?;
        self.as_mut_stream().write_all(res.body().as_ref()).await?;
        self.as_mut_stream().flush().await?;

        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use http::{header, Method, StatusCode};
    use tokio::io::AsyncReadExt;

    use crate::testing::test_stream;

    use super::*;

    #[tokio::test]
    async fn parse_request() {
        test_stream(|mut stream| async move {
            stream
                .server
                .write_all(
                    "POST /about HTTP/1.1\r\ncontent-length: 13\r\ncontent-type: application/x-www-form-urlencoded\r\n\r\nuser=john_doe"
                        .as_bytes(),
                )
                .await
                .unwrap();
            stream.server.flush().await?;

            let mut http_impl = HTTP1::from_stream(stream.client);
            let req = http_impl.parse_request().await.unwrap().unwrap();

            assert_eq!(req.uri(), "/about");
            assert_eq!(req.method(), Method::POST);
            assert_eq!(
                req.headers()
                    .get(header::CONTENT_TYPE)
                    .as_ref()
                    .expect("Content-Type header should be present")
                    .to_str()
                    .unwrap(),
                "application/x-www-form-urlencoded"
            );
            assert_eq!(req.body(), "user=john_doe".as_bytes());

            Ok(())
        })
        .await
        .unwrap();
    }

    #[tokio::test]
    async fn send_request() {
        let buf = test_stream(|mut stream| async move {
            let mut http_impl = HTTP1::from_stream(stream.client);
            http_impl
                .send_request(
                    http::Request::builder()
                        .uri("/about")
                        .method(Method::POST)
                        .header(header::CONTENT_TYPE, "application/x-www-form-urlencoded")
                        .body("user=john_doe".as_bytes().to_vec())
                        .unwrap(),
                )
                .await?;

            http_impl.as_mut_stream().flush().await?;
            drop(http_impl);
            let mut buf = vec![];
            stream.server.read_to_end(&mut buf).await.unwrap();
            Ok(buf)
        })
        .await
        .unwrap();

        assert_eq!(
            std::str::from_utf8(&buf).unwrap(),
            "POST /about HTTP/1.1\r\ncontent-type: application/x-www-form-urlencoded\r\n\r\nuser=john_doe"
        );
    }

    #[tokio::test]
    async fn parse_response() {
        test_stream(|mut stream| async move {
            stream
                .server
                .write_all(
                    "HTTP/1.1 302 Found\r\ncontent-type: text/html\r\n\r\n<h1>Hello world!</h1>"
                        .as_bytes(),
                )
                .await
                .unwrap();
            stream.server.flush().await?;

            let mut http_impl = HTTP1::from_stream(stream.client);
            let res = http_impl.parse_response().await.unwrap().unwrap();

            assert_eq!(res.status(), StatusCode::FOUND);
            assert_eq!(
                res.headers()
                    .get(header::CONTENT_TYPE)
                    .as_ref()
                    .expect("Content-Type header should be present")
                    .to_str()
                    .unwrap(),
                "text/html"
            );
            assert_eq!(res.body(), "<h1>Hello world!</h1>".as_bytes());

            Ok(())
        })
        .await
        .unwrap();
    }

    #[tokio::test]
    async fn send_response() {
        let buf = test_stream(|mut stream| async move {
            let mut http_impl = HTTP1::from_stream(stream.client);
            http_impl
                .send_response(
                    http::Response::builder()
                        .status(StatusCode::FOUND)
                        .header(header::CONTENT_TYPE, "text/html")
                        .body("<h1>Hello world!</h1>".as_bytes().to_vec())
                        .unwrap(),
                )
                .await?;

            http_impl.as_mut_stream().flush().await?;
            drop(http_impl);
            let mut buf = vec![];
            stream.server.read_to_end(&mut buf).await.unwrap();
            Ok(buf)
        })
        .await
        .unwrap();

        assert_eq!(
            std::str::from_utf8(&buf).unwrap(),
            "HTTP/1.1 302 Found\r\ncontent-type: text/html\r\n\r\n<h1>Hello world!</h1>"
        );
    }
}
