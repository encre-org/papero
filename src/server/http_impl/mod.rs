// TODO: Use bytes or http_body instead of Vec<u8>?
pub type Request = http::Request<Vec<u8>>;
pub type Response = http::Response<Vec<u8>>;

mod http1;

#[cfg(feature = "http2")]
mod http2;

#[cfg(feature = "http3")]
mod http3;

pub(crate) use http1::HTTP1;

#[cfg(feature = "http2")]
pub(crate) use http2::HTTP2;

#[cfg(feature = "http3")]
pub(crate) use http3::HTTP3;

type Result<T> = std::result::Result<T, crate::error::HttpImplError>;

macro_rules! def_method {
    (fn $name:ident( self, $( $arg_name:ident: $arg_type:ty ),* ) -> $ret:ty) => {
        #[inline(always)]
        pub fn $name(self, $($arg_name: $arg_type),*) -> $ret {
            match self {
                Self::HTTP1(http) => http.$name($($arg_name),*),

                #[cfg(feature = "http2")]
                Self::HTTP2(http) => http.$name($($arg_name),*),

                #[cfg(feature = "http3")]
                Self::HTTP3(http) => http.$name($($arg_name),*),
            }
        }
    };
    (fn $name:ident( &self, $( $arg_name:ident: $arg_type:ty ),* ) -> $ret:ty) => {
        #[inline(always)]
        pub fn $name(&self, $($arg_name: $arg_type),*) -> $ret {
            match self {
                Self::HTTP1(http) => http.$name($($arg_name),*),

                #[cfg(feature = "http2")]
                Self::HTTP2(http) => http.$name($($arg_name),*),

                #[cfg(feature = "http3")]
                Self::HTTP3(http) => http.$name($($arg_name),*),
            }
        }
    };
    (fn $name:ident( &mut self, $( $arg_name:ident: $arg_type:ty ),* ) -> $ret:ty) => {
        #[inline(always)]
        pub fn $name(&mut self, $($arg_name: $arg_type),*) -> $ret {
            match self {
                Self::HTTP1(http) => http.$name($($arg_name),*),

                #[cfg(feature = "http2")]
                Self::HTTP2(http) => http.$name($($arg_name),*),

                #[cfg(feature = "http3")]
                Self::HTTP3(http) => http.$name($($arg_name),*),
            }
        }
    };
    (async fn $name:ident(&mut self, $( $arg_name:ident: $arg_type:ty ),* ) -> $ret:ty) => {
        #[inline(always)]
        pub async fn $name(&mut self, $($arg_name: $arg_type),*) -> $ret {
            match self {
                Self::HTTP1(http) => http.$name($($arg_name),*).await,

                #[cfg(feature = "http2")]
                Self::HTTP2(http) => http.$name($($arg_name),*).await,

                #[cfg(feature = "http3")]
                Self::HTTP3(http) => http.$name($($arg_name),*).await,
            }
        }
    };
    (async fn $name:ident<T: AsRef<[u8]> + Send>( &mut self, $( $arg_name:ident: $arg_type:ty ),* ) -> $ret:ty) => {
        #[inline(always)]
        pub async fn $name<T: AsRef<[u8]> + Send>(&mut self, $($arg_name: $arg_type),*) -> $ret {
            match self {
                Self::HTTP1(http) => http.$name($($arg_name),*).await,

                #[cfg(feature = "http2")]
                Self::HTTP2(http) => http.$name($($arg_name),*).await,

                #[cfg(feature = "http3")]
                Self::HTTP3(http) => http.$name($($arg_name),*).await,
            }
        }
    };
}

pub enum HttpImpl {
    HTTP1(HTTP1),

    #[cfg(feature = "http2")]
    HTTP2(HTTP2),

    #[cfg(feature = "http3")]
    HTTP3(HTTP3),
}

impl HttpImpl {
    def_method!(async fn parse_request(&mut self,) -> Result<Option<Request>>);
    def_method!(async fn send_request(&mut self, req: Request) -> Result<()>);
    def_method!(async fn parse_response(&mut self,) -> Result<Option<Response>>);
    def_method!(async fn send_response<T: AsRef<[u8]> + Send>(&mut self, res: http::Response<T>) -> Result<()>);
}
