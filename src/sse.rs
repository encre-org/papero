use crate::{
    server::{
        http_impl::{HttpImpl, Request},
        tls::MaybeTlsStream,
    },
    stream::ConvertStream,
    Stream,
};
use http::{header, Version};
use std::time::Duration;
use tokio::io::AsyncWriteExt;

pub use futures_util::{SinkExt, StreamExt};
pub use tokio_tungstenite::tungstenite::Message;

pub struct SSEStream {
    next_id: usize,
    stream: MaybeTlsStream,
}

impl SSEStream {
    pub async fn send(&mut self, event: Event) -> crate::Result<()> {
        match event {
            Event::Data { event, data } => {
                self.stream.write_all(b"event: ").await?;
                self.stream.write_all(&event.into_bytes()).await?;
                self.stream.write_all(b"\nid: ").await?;
                self.stream
                    .write_all(&self.next_id.to_string().into_bytes())
                    .await?;
                self.stream.write_all(b"\ndata: ").await?;
                self.stream
                    .write_all(&data.replace('\n', "\ndata: ").into_bytes())
                    .await?;
                self.stream.write_all(b"\n\n").await?;
            }
            Event::Comment(text) => {
                self.stream.write_all(b": ").await?;
                self.stream.write_all(&text.into_bytes()).await?;
                self.stream.write_all(b"\n\n").await?;
            }
        }

        self.next_id += 1;
        Ok(())
    }

    pub async fn set_retry(&mut self, retry: Duration) -> crate::Result<()> {
        self.stream.write_all(b"retry: ").await?;
        self.stream
            .write_all(&retry.as_millis().to_string().into_bytes())
            .await?;
        self.stream.write_all(b"\n\n").await?;
        Ok(())
    }
}

pub enum Event {
    Data { event: String, data: String },
    Comment(String),
}

impl Event {
    pub fn new_data<A: Into<String>, B: Into<String>>(event: A, data: B) -> Self {
        Self::Data {
            event: event.into(),
            data: data.into(),
        }
    }

    pub fn new_comment<T: Into<String>>(text: T) -> Self {
        Self::Comment(text.into())
    }
}

#[async_trait::async_trait]
impl ConvertStream for SSEStream {
    async fn convert_stream(stream: &mut Stream, _req: Request) -> crate::Result<SSEStream> {
        if stream.http_version() == Version::HTTP_11 {
            let res = http::Response::builder()
                .header(header::CONTENT_TYPE, "text/event-stream")
                .header(header::CACHE_CONTROL, "no-cache")
                .body(vec![])
                .expect("failed to make SSE response from request"); // TODO: Error management
            stream.send(res).await?;

            #[allow(irrefutable_let_patterns)]
            let HttpImpl::HTTP1(http_impl) = stream.into_http() else {
                unreachable!("HTTP version was already checked before");
            };

            Ok(SSEStream {
                next_id: 0,
                stream: http_impl.into_stream(),
            })
        } else {
            unimplemented!("SSE is not yet implemented when not using HTTP/1");
        }
    }
}
