use http::{HeaderMap, Version};
use tokio_util::sync::CancellationToken;

use crate::server::http_impl::{HttpImpl, Request};

#[async_trait::async_trait]
pub trait SendToStream {
    async fn send_to_stream(self, stream: &mut Stream) -> crate::Result<()>;
}

#[async_trait::async_trait]
impl<T: SendToStream + Send + ?Sized> SendToStream for Box<T> {
    #[inline]
    async fn send_to_stream(self, stream: &mut Stream) -> crate::Result<()> {
        self.send_to_stream(stream).await
    }
}

#[async_trait::async_trait]
pub trait ConvertStream
where
    Self: Sized,
{
    async fn convert_stream(stream: &mut Stream, req: Request) -> crate::Result<Self>;
}

#[derive(Debug, Clone, PartialEq, Default)]
pub(crate) struct StreamExtra {
    pub(crate) headers: HeaderMap,
}

/// A stream connecting the client to the server.
///
/// It can be manipulated using helper functions like [`Stream::send`] or [`Stream::convert_into`].
///
/// ### Stream conversion
///
/// One thing you may want when building a server is changing the protocol used by the stream. To
/// do that, [`Stream`] includes a carefully-crafted API based on the [`ConvertStream`] trait.
/// This trait is implemented for some structures wrapping a stream and its corresponding protocol
/// methods (e.g [`WebSocketStream`] or [`SSEStream`]).
///
/// When building such a structure, you may want to take ownership of the stream to fully wrap it.
/// In this case you can use the [`Stream::into_http`] method. But as a user of such a structure,
/// keep in mind that **converting a stream could make it unusable afterwards**. However, you can
/// use a stream as long you don't try to convert it twice, e.g you can send multiple responses if
/// you need.
///
/// [`WebSocketStream`]: crate::ws::WebSocketStream
/// [`SSEStream`]: crate::sse::SSEStream
pub struct Stream {
    inner: Option<HttpImpl>,
    pub(crate) cancel_timeout_token: CancellationToken,
    extra: StreamExtra,
}

impl Stream {
    pub(crate) fn from_http(inner: HttpImpl, cancel_timeout_token: CancellationToken) -> Self {
        Self {
            inner: Some(inner),
            cancel_timeout_token,
            extra: StreamExtra::default(),
        }
    }

    pub(crate) fn get_mut_extra(&mut self) -> &mut StreamExtra {
        &mut self.extra
    }

    pub fn http_version(&self) -> Version {
        match self.inner.as_ref().expect("the stream was converted before so it could not be used anymore (see `Stream` documentation)") {
            HttpImpl::HTTP1(_) => Version::HTTP_11,

            #[cfg(feature = "http2")]
            HttpImpl::HTTP2(_) => Version::HTTP_2,

            #[cfg(feature = "http3")]
            HttpImpl::HTTP3(_) => Version::HTTP_3,
        }
    }

    pub fn as_http(&self) -> &HttpImpl {
        self.inner.as_ref().expect("the stream was converted before so it could not be used anymore (see `Stream` documentation)")
    }

    pub fn as_mut_http(&mut self) -> &mut HttpImpl {
        self.inner.as_mut().expect("the stream was converted before so it could not be used anymore (see `Stream` documentation)")
    }

    pub fn into_http(&mut self) -> HttpImpl {
        self.inner.take().expect("the stream was converted before so it could not be used anymore (see `Stream` documentation)")
    }

    #[inline]
    pub async fn send<T: SendToStream>(&mut self, res: T) -> crate::Result<()> {
        res.send_to_stream(self).await
    }

    #[inline]
    pub async fn convert_into<T: ConvertStream>(&mut self, req: Request) -> crate::Result<T> {
        self.cancel_timeout_token.cancel();
        T::convert_stream(self, req).await
    }
}

#[async_trait::async_trait]
impl<T: AsRef<[u8]> + Send> SendToStream for http::Response<T> {
    async fn send_to_stream(mut self, stream: &mut crate::Stream) -> crate::Result<()> {
        for extra_header in &stream.extra.headers {
            // If the header is already set, it won't be overridden
            self.headers_mut()
                .entry(extra_header.0)
                .or_insert_with(|| extra_header.1.clone());
        }

        // Set Content-Length header
        // TODO: Follow https://httpwg.org/specs/rfc9112.html#message.body.length
        // TODO: Avoid unwrapping
        let body_len_string = self.body().as_ref().len().to_string();
        self.headers_mut()
            .entry(http::header::CONTENT_LENGTH)
            .or_insert_with(|| http::HeaderValue::from_str(&body_len_string).unwrap());

        stream
            .as_mut_http()
            .send_response(self)
            .await
            .map_err(|e| e.into())
    }
}
