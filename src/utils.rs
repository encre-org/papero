use http::StatusCode;
use std::{fs, path::Path};

use crate::{server::http_impl::Request, Stream};

/// Send an HTTP response containing a redirection to `url`
pub async fn redirect_to(stream: &mut Stream, url: &str) -> crate::Result<()> {
    stream
        .send(
            http::Response::builder()
                .status(StatusCode::SEE_OTHER)
                .header(http::header::LOCATION, url)
                .body(vec![])
                .unwrap(),
        )
        .await
}

/// Send an HTTP response containing the contents of a file.
///
/// The file is chosen based on the relative path in `dir` on the filesystem
/// from `url_path` in the request URL.
///
/// For instance, using `serve_static(&mut stream, &req, "/", "assets")`
/// will serve all files inside the `assets` directory accessible from the root url, eg. querying
/// `https://example.com/image.png` will try to read and send the file `assets/image.png`
/// on the filesystem.
///
/// If the file is not found a 404 response will be automatically generated.
pub async fn serve_static<P: AsRef<Path>>(
    stream: &mut Stream,
    req: &Request,
    prefix_url_path: &str,
    dir: P,
) -> crate::Result<()> {
    let inner_path = req.uri().path().strip_prefix(prefix_url_path).unwrap();
    let complete_path = dir.as_ref().join(inner_path);

    if let Ok(file_content) = fs::read(&complete_path) {
        stream
            .send(
                http::Response::builder()
                    .header(
                        http::header::CONTENT_TYPE,
                        mime_guess::from_path(complete_path)
                            .first_raw()
                            .expect("failed to guess asset mime type"),
                    )
                    .body(file_content)
                    .expect("failed to build response"),
            )
            .await
    } else {
        stream
            .send(
                http::Response::builder()
                    .status(StatusCode::NOT_FOUND)
                    .body(b"404 Not Found".to_vec())
                    .expect("failed to build response"),
            )
            .await
    }
}
