use std::{net::{SocketAddr, ToSocketAddrs}, path::PathBuf};

/// The configuration of server-side TLS.
///
/// You can generate a self-signed certificate and key (with 1 year expiration) using:
/// `openssl req -x509 -newkey rsa:4096 -nodes -keyout key.pem -out cert.pem -days 365 -subj '/CN=localhost'`.
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct TlsConfig {
    /// Path to the certificate file.
    pub cert_path: PathBuf,

    /// Path to the private key file.
    pub key_path: PathBuf,
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Config {
    /// Configuration of TLS.
    ///
    /// If it is not `None`, only a server supporting TLS will be executed and will serve all HTTPS
    /// requests.
    pub(crate) tls: Option<TlsConfig>,

    /// The address on which the HTTP server (without TLS support) will listen for incoming
    /// connections.
    ///
    /// Typically in development you'll want either `127.0.0.1:<some_port>`, `localhost:<some_port>`
    /// or `0.0.0.0:<some_port>`.
    pub(crate) http_listen_addr: Vec<SocketAddr>,

    /// The address on which the HTTPS server (the HTTP server supporting TLS) will listen for incoming
    /// connections.
    ///
    /// Works only if TLS is enabled (ie the `tls` field is not `None`).
    ///
    /// You may want to set `redirect_to_https` if you want automatic HTTP to HTTPS redirection.
    ///
    /// Typically in development you'll want either `127.0.0.1:<some_port>`, `localhost:<some_port>`
    /// or `0.0.0.0:<some_port>`.
    pub(crate) https_listen_addr: Vec<SocketAddr>,

    /// The address on which the HTTP/3 server (the HTTP server using QUIC) will listen for incoming
    /// connections.
    ///
    /// **HTTP/3 support is highly experimental and should not be used in production yet**
    ///
    /// For use in browsers, you need to set an IPv6 address.
    ///
    /// Typically in development you'll want `[::]:<some_port>`.
    pub(crate) http3_listen_addr: Option<Vec<SocketAddr>>,

    /// Whether the HTTP server should send a redirection to the HTTPS server on connection.
    ///
    /// The value is the complete HTTPS address to redirect to (it is not possible to guess it),
    /// e.g `https://localhost:<some_port>`.
    ///
    /// Works only if TLS is enabled (ie the `tls` field is not `None`).
    pub(crate) redirect_to_https: Option<String>,
}

impl Config {
    pub fn new() -> Self {
        Self::default()
    }

    pub fn with_tls(mut self, tls_config: TlsConfig) -> Self {
        self.tls = Some(tls_config);
        self
    }

    pub fn with_http_listen_addr<A: ToSocketAddrs>(mut self, addr: A) -> Self {
        self.http_listen_addr = addr.to_socket_addrs().expect("failed to convert addresses").collect();
        self
    }

    pub fn with_https_listen_addr<A: ToSocketAddrs>(mut self, addr: A) -> Self {
        self.https_listen_addr = addr.to_socket_addrs().expect("failed to convert addresses").collect();
        self
    }

    pub fn with_http3_listen_addr<A: ToSocketAddrs>(mut self, addr: A) -> Self {
        self.http3_listen_addr = Some(addr.to_socket_addrs().expect("failed to convert addresses").collect());
        self
    }

    pub fn with_redirect_to_https(mut self, url: Option<String>) -> Self {
        self.redirect_to_https = url;
        self
    }
}

impl Default for Config {
    fn default() -> Self {
        Self {
            tls: None,
            http_listen_addr: "127.0.0.1:80".to_socket_addrs().expect("failed to convert addresses").collect(),
            https_listen_addr: "127.0.0.1:443".to_socket_addrs().expect("failed to convert addresses").collect(),
            http3_listen_addr: None,
            redirect_to_https: Some(String::from("https://localhost:443")),
        }
    }
}
